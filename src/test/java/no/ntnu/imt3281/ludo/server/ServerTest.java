package no.ntnu.imt3281.ludo.server;

import io.jsonwebtoken.Claims;
import no.ntnu.imt3281.ludo.client.ServerConnection;
import org.json.simple.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.parseInt;
import static org.junit.Assert.*;

/**
 * Tests for Server-class
 */
public class ServerTest {

    // Database, server and clients connection to server
    private static Connection connection;
    private static Server server;
    private static ServerConnection serverConnection;
    private final static String EXPIREDTOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NzIwMzQ0NTAsImV4cCI6MTU3MjEyMDg1MCwic3ViIjoidGVzdE5hbWUifQ.hkoLmenqp6D54sndqXz5OjHffgN7RjDrSUmXn5FrTX4";

    /**
     * Establish connection to fresh test-DB(empty tables)
     * Starts server and connection to server
     */
    @BeforeClass
    public static void setUp(){

        connection = getFreshDB();
        server = new Server(connection);
        serverConnection = new ServerConnection();
    }

    /**
     * Tests communication between client(Connection-object) and Server
     * as well as server-functions towards the database
     *
     * The method addUserToDB adds a user to the DB
     *   Returns: true if user was added, throws a UserNameExistsException if username already exists in the DB
     *
     * The method getUserFromDb retrieves a user from the DB
     *  Returns: JSONObject, a user, or null if user does not exist
     *
     * The method removeUserFromDB should remove a user from DB
     *   Returns: true if removal was successful, false otherwise
     *
     * The method userNameTaken checks whether a userName exists in the DB
     *   Returns: True if userName exists, false otherwise
     *
     * All other functionality-testing is done through socket-communication with JSONObject
     */
    @Test
    public void testServer(){

        boolean exceptionThrown = false;
        String answer;
        JSONObject message;
        JSONObject received = null;

        ////////////// Test basic communication //////////////
        //// Send valid action to server
        // Create message
        message = new JSONObject();
        message.put("Action", "Test");

        // Communicate with server
        try {
            // Send message to server
            serverConnection.send(message.toString());
            // Read answer from server
            answer = serverConnection.read();
            received = server.stringToJSON(answer);

        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that server returned expected message:
        assertFalse(exceptionThrown);
        assertEquals("This message implies communication ok", received.get("Message"));

        // Reset
        exceptionThrown = false;

        //// Send invalid action to server
        message = new JSONObject();
        message.put("Action", "Undefined Action in Server");

        // Communicate with server
        try {
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);

        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that server returned expected message:
        assertFalse(exceptionThrown);
        assertEquals("invalidAction", received.get("Action"));

        ////////////// Test creating new user with userName Siri //////////////
        // Assert Siri does not already exist
        assertFalse(server.userExists("Siri"));

        //// Try creating Siri, should work
        message = new JSONObject();
        message.put("Action", "new user");
        message.put("UserName", "Siri");
        message.put("Password", "SirisPassword");

        // Communicate with server
        try {
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown, Action is newUserOK and that Siri exists
        assertFalse(exceptionThrown);
        assertEquals("newUserOK", received.get("Action"));
        assertTrue(server.userExists("Siri"));

        //// Try creating Siri again, should not work

        // Communicate with server
        try {
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown, Siri was not added and that Siri still exists
        assertFalse(exceptionThrown);
        assertEquals("newUserFAIL", received.get("Action"));
        assertTrue(server.userExists("Siri"));


        ////////////// Test retrieving users //////////////

        //// Retrieve Siri from DB, check that values are correct

        // Try retrieving Siri
        JSONObject user = server.getUserFromDb("Siri");
        // Assert user is not empty
        assertNotNull(user);

        // Get values
        String userName = user.get("UserName").toString();
        String token = user.get("Token").toString();
        int noOfGames = parseInt(user.get("noOfGames").toString());
        int noOfVictories = parseInt(user.get("noOfVictories").toString());

        // Assert values are correct
        assertEquals("Siri", userName);
        assertEquals(0, noOfGames);
        assertEquals(0, noOfVictories);
        assertTrue(token.length() > 0);

        //// Try retrieving Jake(should not work, does not exist)
        // Try retrieving Jake
        user = server.getUserFromDb("Jake");
        // Assert user is empty
        assertNull(user);

        // Log out
        message = new JSONObject();
        message.put("Action", "logout");
        try{
            serverConnection.send(message.toString());
        } catch(IOException ignored){}
        serverConnection = new ServerConnection();

        ////////////// Test loginManually //////////////

        //// Try logging in as Siri with correct username/password combination
        message = new JSONObject();
        message.put("Action", "loginManually");
        message.put("UserName", "Siri");
        message.put("Password", "SirisPassword");

        // Communicate with server
        try {
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that Siri was logged in
        assertFalse(exceptionThrown);
        assertEquals("loginManualOK", received.get("Action"));

        //// Try logging in as Siri with incorrect password
        message = new JSONObject();
        message.put("Action", "loginManually");
        message.put("UserName", "Siri");
        message.put("Password", "wrongPassword");

        // Communicate with server
        try {
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that Siri was not logged in
        assertFalse(exceptionThrown);
        assertEquals("loginManualFAIL", received.get("Action"));

        // Make sure validCombination returns false when user does not exist(without server calling userExists() first)
        assertFalse(server.validCombination("nonExistingName","Password"));

        //// Try logging in as non-existing user
        message = new JSONObject();
        message.put("Action", "loginManually");
        message.put("UserName", "nonExistingUserName");
        message.put("Password", "nonExistingPassword");

        // Communicate with server
        try {
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that non-existing was not logged in
        assertFalse(exceptionThrown);
        assertEquals("loginManualFAIL", received.get("Action"));



        ////////////// Test automatic login(with token) ////////////////
        //// Try automatic-login for Siri
        // Retrieve Siris token from DB
        token = server.getToken("Siri");

        message = new JSONObject();
        message.put("Action", "loginAuto");
        message.put("Token", token);

        // Communicate with server
        try {
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that Siri was logged in(Action is loginAutoOK)
        assertFalse(exceptionThrown);
        assertEquals("loginAutoOK", received.get("Action"));

        // Log out
        message = new JSONObject();
        message.put("Action", "logout");
        try{
            serverConnection.send(message.toString());
        } catch(IOException ignored){}
        serverConnection = new ServerConnection();

        //// Try automatic-login for Siri with expired token
        message = new JSONObject();
        message.put("Action", "loginAuto");
        message.put("Token", EXPIREDTOKEN);

        // Communicate with server
        try {
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that Siri was not logged in(Action is loginAutoFAIL)
        assertFalse(exceptionThrown);
        assertEquals("loginAutoFAIL", received.get("Action"));

        //// Try automatic-login for Siri with a previous token

        String previousToken = "eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NzI0MzQ1NTYsImV4cCI6MTU3MjUyMDk1Niwic3ViIjoiU2lyaSJ9.APv-h4n-Qy3c1649DFXPCwAMRj8hr55m4dvy0YG5ukM";
        message = new JSONObject();
        message.put("Action", "loginAuto");
        message.put("Token", previousToken);

        // Communicate with server
        try {
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that Siri was not logged in(Action is loginAutoFAIL)
        assertFalse(exceptionThrown);
        assertEquals("loginAutoFAIL", received.get("Action"));

        ////////////// Try removing users //////////////
        //// Try removing Siri
        boolean removed;

        // Assert user exists and remove user
        assertTrue(server.userExists("Siri"));
        removed = server.removeUserFromDB("Siri");

        // Assert user was removed and that Siri does no longer exist
        assertTrue(removed);
        assertFalse(server.userExists("Siri"));

        //// Try removing Siri AGAIN and assert Siri was not removed
        removed = server.removeUserFromDB("Siri");
        assertFalse(removed);


        ////////////// Test top 10 lists //////////////

        // Add new users Siri, Peter and Maria
        JSONObject credentials = new JSONObject();
        credentials.put("UserName", "Siri");
        credentials.put("Password", "SirisPassword");

        JSONObject credentials2 = new JSONObject();
        credentials2.put("UserName", "Peter");
        credentials2.put("Password", "PetersPassword");

        JSONObject credentials3 = new JSONObject();
        credentials3.put("UserName", "Maria");
        credentials3.put("Password", "MariasPassword");

        server.newUser(credentials);
        server.newUser(credentials2);
        server.newUser(credentials3);

        // Assert three users exist
        assertTrue(server.userExists("Siri"));
        assertTrue(server.userExists("Peter"));
        assertTrue(server.userExists("Maria"));

        //// Update game statistics
        // Siri wins three games
        server.updateUserGamesDb("Siri", true);
        server.updateUserGamesDb("Siri", true);
        server.updateUserGamesDb("Siri", true);
        // Peter wins one game and loses three games
        server.updateUserGamesDb("Peter", true);
        server.updateUserGamesDb("Peter", false);
        server.updateUserGamesDb("Peter", false);
        server.updateUserGamesDb("Peter", false);
        // Maria loses one game
        server.updateUserGamesDb("Maria", false);

        // Log in as Siri and refresh
        ServerConnection sirisConnection = new ServerConnection();
        try {
            message = new JSONObject();
            message.put("Action", "loginManually");
            message.put("UserName", "Siri");
            message.put("Password", "SirisPassword");
            sirisConnection.send(message.toString());
            answer = sirisConnection.read();
            answer = sirisConnection.read();
            message = new JSONObject();
            message.put("Action", "refresh");
            sirisConnection.send(message.toString());
            answer = sirisConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that list was received
        assertFalse(exceptionThrown);
        assertEquals("refresh", received.get("Action"));

        List<JSONObject> top10Games = (List) received.get("Games");
        List<JSONObject> top10Victories = (List) received.get("Victories");

        // Assert that order and amount of games are correct:
        assertEquals("Peter", top10Games.get(0).get("UserName"));
        assertEquals(4L, top10Games.get(0).get("noOfGames"));
        assertEquals("Siri", top10Games.get(1).get("UserName"));
        assertEquals(3L, top10Games.get(1).get("noOfGames"));
        assertEquals("Maria", top10Games.get(2).get("UserName"));
        assertEquals(1L, top10Games.get(2).get("noOfGames"));

        // Assert that order and amount of wins are correct:
        assertEquals("Siri", top10Victories.get(0).get("UserName"));
        assertEquals(3L, top10Victories.get(0).get("noOfVictories"));
        assertEquals("Peter", top10Victories.get(1).get("UserName"));
        assertEquals(1L, top10Victories.get(1).get("noOfVictories"));
        assertEquals("Maria", top10Victories.get(2).get("UserName"));
        assertEquals(0L, top10Victories.get(2).get("noOfVictories"));


        ////////////// Test changing userName //////////////
        // Register new user Siri
        message = new JSONObject();
        message.put("Action", "loginAuto");
        message.put("Token", token);
        try {
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            answer = serverConnection.read();
        } catch(IOException ignored){}

        //// Try changing Siris userName to Janice(should work)
        message = new JSONObject();
        message.put("Action","change username");
        message.put("NewUserName","Janice");

        // Communicate with server
        try {
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown, userName was changed, Janice exists and that Siri no longer exists
        assertFalse(exceptionThrown);
        assertEquals("nameChangeOK", received.get("Action").toString());
        assertTrue(server.userExists("Janice"));
        assertFalse(server.userExists("Siri"));

        //// Try changing Marias name to Janice(should not work, Janice already exists)
        message = new JSONObject();
        message.put("Action","change username");
        message.put("UserName","Maria");
        message.put("NewUserName","Janice");

        // Communicate with server
        try {
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown, userName was not changed and that both Maria and Janice still exist
        assertFalse(exceptionThrown);
        assertEquals("nameChangeFAIL", received.get("Action").toString());
        assertTrue(server.userExists("Maria"));
        assertTrue(server.userExists("Janice"));


        ////////////// Test changing password //////////////
        //// Try changing Janices password(should work)
        message = new JSONObject();
        message.put("Action", "change password");
        message.put("Password", "SirisPassword");
        message.put("NewPassword", "NewPassword");

        // Communicate with server
        try {
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that password was changed
        assertFalse(exceptionThrown);
        assertEquals("passwordChangeOK", received.get("Action").toString());

        // Log out
        message = new JSONObject();
        message.put("Action", "logout");
        try{
            serverConnection.send(message.toString());
        } catch(IOException ignored){}
        serverConnection = new ServerConnection();

        //// Try logging in with new password
        message = new JSONObject();
        message.put("Action", "loginManually");
        message.put("UserName", "Janice");
        message.put("Password", "NewPassword");

        // Communicate with server
        try {
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that Maria was logged in
        assertFalse(exceptionThrown);
        assertEquals("loginManualOK", received.get("Action"));

        //// Try changing Janices password with wrong (old) password
        message = new JSONObject();
        message.put("Action", "change password");
        message.put("Password", "wrongPassword");
        message.put("NewPassword", "NewPassword");

        // Communicate with server
        try {
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that password was not changed
        assertFalse(exceptionThrown);
        assertEquals("passwordChangeFAIL", received.get("Action").toString());


        ////////////// Test uploading picture //////////////
        // An image is sent by converting byte-array to a string, create random string to simulate a picture
        String random = "lsadcs9fs98czx7asf9f";

        // Construct message
        message = new JSONObject();
        message.put("Action", "change picture");
        message.put("Image", random);

        // Send message
        try{
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException e){
            exceptionThrown = true;
        }
        // Assert exception was not thrown
        assertFalse(exceptionThrown);
        // Assert message action is 'pictureChangeOK'
        assertEquals("pictureChangeOK", received.get("Action"));


        ////////////// Test joining random game //////////////
        //// Join random game as Janice(because she logged in lastly on this computer)
        try {
            message = new JSONObject();
            message.put("Action", "joinRandomGame");
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch (IOException e) {
            exceptionThrown = true;
        }
        // Assert exception was not thrown
        assertFalse(exceptionThrown);
        // Assert message is playerJoined, that one player is in game, that the name is Maria, gameId is 1 and timer is 0
        assertEquals("playerJoined", received.get("Action"));
        List playersJoined = (List) received.get("PlayersJoined");
        assertEquals(1, playersJoined.size());
        assertEquals("Janice", playersJoined.get(0));
        assertEquals(1, Integer.parseInt(received.get("GameID").toString()));
        assertEquals(0, Integer.parseInt(received.get("Timer").toString()));

        //// Join another random game as Janice(She should not join her own game -> a new game with id 2 should be created)
        try {
            message = new JSONObject();
            message.put("Action", "joinRandomGame");
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch (IOException e) {
            exceptionThrown = true;
        }
        // Assert exception was not thrown
        assertFalse(exceptionThrown);
        // Assert message is playerJoined, that one player is in game, that the name is Maria and gameId is 2
        assertEquals("playerJoined", received.get("Action"));
        playersJoined = (List) received.get("PlayersJoined");
        assertEquals(1, playersJoined.size());
        assertEquals("Janice", playersJoined.get(0));
        assertEquals(2, Integer.parseInt(received.get("GameID").toString()));

        //// Send gamechat
        try {
            message = new JSONObject();
            message.put("Action", "gameChat");
            message.put("GameID", "1");
            message.put("Message", "Janices message");
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch (IOException e) {
            exceptionThrown = true;
        }
        // Assert exception was not thrown, Action is 'gameChat' and content is correct
        assertFalse(exceptionThrown);
        assertEquals("gameChat", received.get("Action").toString());
        assertEquals("Janice", received.get("Sender").toString());
        assertEquals("1", received.get("GameID").toString());
        assertEquals("Janices message", received.get("Message").toString());

        //// Log in as Peter and join random game, should join Janices game
        ServerConnection petersConnection = new ServerConnection();
        message = new JSONObject();
        message.put("Action", "loginManually");
        message.put("UserName", "Peter");
        message.put("Password", "PetersPassword");
        try {
            petersConnection.send(message.toString());
            answer = petersConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException e){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that Peter was logged in
        assertFalse(exceptionThrown);
        assertEquals("loginManualOK", received.get("Action"));

        // Join random game as Peter
        try {
            message = new JSONObject();
            message.put("Action", "joinRandomGame");
            petersConnection.send(message.toString());
            answer = petersConnection.read();
            received = server.stringToJSON(answer);
        } catch (IOException e) {
            exceptionThrown = true;
        }
        // Assert exception was not thrown
        assertFalse(exceptionThrown);
        // Assert message action is 'playerJoined', two players are in game, the names are Janice and Peter and gameId is 1
        assertEquals("playerJoined", received.get("Action"));
        playersJoined = (List) received.get("PlayersJoined");
        assertEquals(2, playersJoined.size());
        assertEquals("Janice", playersJoined.get(0));
        assertEquals("Peter", playersJoined.get(1));
        assertEquals(1, Integer.parseInt(received.get("GameID").toString()));


        ////////////// Test global-chat and chatroom-functionality //////////////
        //// Send global chat message as Peter
        try{
            message = new JSONObject();
            message.put("Action", "chat");
            message.put("ChatName", "Global Chat");
            message.put("Message", "Hi! I'm Peter.");
            petersConnection.send(message.toString());
            answer = petersConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException e){
            exceptionThrown = true;
        }
        // Assert exception was not thrown
        assertFalse(exceptionThrown);
        // Assert message action is 'Global chat', Sender is Peter and Message is correct
        assertEquals("chat", received.get("Action"));
        assertEquals("Global Chat", received.get("ChatName"));
        assertEquals("Peter", received.get("Sender"));
        assertEquals("Hi! I'm Peter.", received.get("Message"));

        //// Send chat-message to non-existing chatroom
        try{
            message = new JSONObject();
            message.put("Action", "chat");
            message.put("ChatName", "non-existing");
            message.put("Message", "some message");
            petersConnection.send(message.toString());
            answer = petersConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that Action is 'msgFAIL'
        assertFalse(exceptionThrown);
        assertEquals("msgFAIL", received.get("Action").toString());

        //// Create chatroom as Peter
        try{
            message = new JSONObject();
            message.put("Action", "createChat");
            message.put("ChatName", "Peters chat");
            petersConnection.send(message.toString());
            answer = petersConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException e){
            exceptionThrown = true;
        }
        // Assert exception was not thrown, that Action is 'createChatOK' and that the chatName is correct
        assertFalse(exceptionThrown);
        assertEquals("createChatOK", received.get("Action").toString());
        assertEquals("Peters chat", received.get("ChatName").toString());

        //// Create ChatRoom again(with the same name, should not work)
        try{
            message = new JSONObject();
            message.put("Action", "createChat");
            message.put("ChatName", "Peters chat");
            petersConnection.send(message.toString());
            answer = petersConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException e){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that Action is 'createChatFAIL'
        assertFalse(exceptionThrown);
        assertEquals("createChatFAIL", received.get("Action").toString());

        //// Try joining Peters chat(should not work, he is already in the chat)
        try{
            message = new JSONObject();
            message.put("Action", "joinChat");
            message.put("ChatName", "Peters chat");
            petersConnection.send(message.toString());
            answer = petersConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that Action is 'joinChatOK'
        assertFalse(exceptionThrown);
        assertEquals("joinChatFAIL", received.get("Action").toString());

        //// Try leaving Peters chat and rejoin(should work)
        try{
            message = new JSONObject();
            message.put("Action", "leaveChat");
            message.put("ChatName", "Peters chat");
            petersConnection.send(message.toString());

            message = new JSONObject();
            message.put("Action", "joinChat");
            message.put("ChatName", "Peters chat");
            petersConnection.send(message.toString());
            answer = petersConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that Action is 'joinChatOK'
        assertFalse(exceptionThrown);
        assertEquals("joinChatOK", received.get("Action").toString());
        assertEquals("Peters chat", received.get("ChatName").toString());

        //// Make sure Janice was informed:
        try {
            // That Peter joined Global Chat
            received = server.stringToJSON(serverConnection.read());
            assertEquals("userJoinedChat", received.get("Action").toString());
            assertEquals("Peter", received.get("UserName").toString());
            assertEquals("Global Chat", received.get("ChatName").toString());

            // That Peter joined her game
            received = server.stringToJSON(serverConnection.read());
            assertEquals("playerJoined", received.get("Action").toString());
            playersJoined = (List) received.get("PlayersJoined");
            assertEquals("Janice", playersJoined.get(0));
            assertEquals("Peter", playersJoined.get(1));

            // That Peter sent chat-message in Global Chat
            received = server.stringToJSON(serverConnection.read());
            assertEquals("Peter", received.get("Sender").toString());
            assertEquals("Hi! I'm Peter.", received.get("Message").toString());
        } catch(IOException ignored){}

        //// Join Peters chat as Janice
        try{
            message = new JSONObject();
            message.put("Action", "joinChat");
            message.put("ChatName", "Peters chat");
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that Action is 'joinChatOK'
        assertFalse(exceptionThrown);
        assertEquals("joinChatOK", received.get("Action").toString());

        //// Assert Peter is notified that Janice joined his chat
        try{
            answer = petersConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown
        assertFalse(exceptionThrown);
        // Assert Action is 'userJoinedChat', that it was Janice and correct chat
        assertEquals("userJoinedChat", received.get("Action").toString());
        assertEquals("Janice", received.get("UserName").toString());
        assertEquals("Peters chat", received.get("ChatName").toString());

        //// Send chat-message to Peters chat(as Janice)
        try{
            message = new JSONObject();
            message.put("Action", "chat");
            message.put("ChatName", "Peters chat");
            message.put("Message", "Janices message");
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown, that Action is 'chat', and that sender, message and chatName are correct
        assertFalse(exceptionThrown);
        assertEquals("chat", received.get("Action").toString());
        assertEquals("Janice", received.get("Sender").toString());
        assertEquals("Peters chat", received.get("ChatName").toString());
        assertEquals("Janices message", received.get("Message").toString());

        //// Assert Peter gets Janices message
        try{
            answer = petersConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown
        assertFalse(exceptionThrown);
        // Assert Action is 'chat', Sender is Janice and correct chat
        assertEquals("chat", received.get("Action").toString());
        assertEquals("Janice", received.get("Sender").toString());
        assertEquals("Peters chat", received.get("ChatName").toString());

        //// Try joining a non-existing chatroom, should not work
        try{
            message = new JSONObject();
            message.put("Action", "joinChat");
            message.put("ChatName", "non-existant");
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that Action is 'joinChatOK'
        assertFalse(exceptionThrown);
        assertEquals("joinChatFAIL", received.get("Action").toString());


        ////////////// Test friends //////////////
        //// Try sending friend-request (as Janice) to Peter(should work)
        //Message server
        try{
            message = new JSONObject();
            message.put("Action", "friendRequest");
            message.put("UserToInvite", "Peter");
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown, that Action is 'friendRequestOK' and Message is correct
        assertFalse(exceptionThrown);
        assertEquals("friendRequestOK", received.get("Action").toString());
        assertEquals("friendRequestSent", received.get("Message").toString());

        //// Try sending another friend-request to Peter as Janice(should not work)
        try{
            message = new JSONObject();
            message.put("Action", "friendRequest");
            message.put("UserToInvite", "Peter");
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown, that Action is 'friendRequestFAIL' and Message is correct
        assertFalse(exceptionThrown);
        assertEquals("friendRequestFAIL", received.get("Action").toString());
        assertEquals("alreadyInvited", received.get("Message").toString());

        //// Try accepting friend-request from Janice
        try{
            message = new JSONObject();
            message.put("Action", "friendAccept");
            message.put("UserName", "Janice");
            petersConnection.send(message.toString());
            answer = petersConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown, that Action is 'friendAcceptOK' and UserName is Janice
        assertFalse(exceptionThrown);
        assertEquals("friendAcceptOK", received.get("Action").toString());
        assertEquals("Janice", received.get("UserName").toString());

        //// Try sending another friend-request to Peter as Janice(they are already friends, should not work)
        try{
            message = new JSONObject();
            message.put("Action", "friendRequest");
            message.put("UserToInvite", "Peter");
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown, that Action is 'friendRequestFAIL' and Message is correct
        assertFalse(exceptionThrown);
        assertEquals("friendRequestFAIL", received.get("Action").toString());
        assertEquals("alreadyFriends", received.get("Message").toString());

        //// Try sending friend-request to a non-existing user(should not work)
        //Message server
        try{
            message = new JSONObject();
            message.put("Action", "friendRequest");
            message.put("UserToInvite", "Nonexisting");
            serverConnection.send(message.toString());
            answer = serverConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown, that Action is 'joinChatOK' and Message is correct
        assertFalse(exceptionThrown);
        assertEquals("friendRequestFAIL", received.get("Action").toString());
        assertEquals("userDoesNotExist", received.get("Message").toString());


        //// Try accepting a non-existant friend-request
        try{
            message = new JSONObject();
            message.put("Action", "friendAccept");
            message.put("UserName", "Maria");
            petersConnection.send(message.toString());
            answer = petersConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that Action is 'friendAcceptFAIL'
        assertFalse(exceptionThrown);
        assertEquals("friendAcceptFAIL", received.get("Action").toString());

        //// Try sending friend-request to a user who has already requested you
        // Send friend-request from Peter to Maria
        try{
            message = new JSONObject();
            message.put("Action", "friendRequest");
            message.put("UserToInvite", "Maria");
            petersConnection.send(message.toString());
            answer = petersConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that Action is 'friendRequestOK'
        assertFalse(exceptionThrown);
        assertEquals("friendRequestOK", received.get("Action").toString());

        // Log in as Maria and send friend-request to Peter(should accept request from Peter)
        ServerConnection mariasConnection = new ServerConnection();
        try{
            // Log in
            message = new JSONObject();
            message.put("Action", "loginManually");
            message.put("UserName", "Maria");
            message.put("Password", "MariasPassword");
            mariasConnection.send(message.toString());
            answer = mariasConnection.read();
            received = server.stringToJSON(answer);
            assertEquals("loginManualOK", received.get("Action"));

            // Send friend-request to Peter
            message = new JSONObject();
            message.put("Action", "friendRequest");
            message.put("UserToInvite", "Peter");
            mariasConnection.send(message.toString());
            answer = mariasConnection.read();
            received = server.stringToJSON(answer);

        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown and that Action is 'friendAdded'
        assertFalse(exceptionThrown);
        assertEquals("friendAdded", received.get("Action").toString());
        assertEquals("Peter", received.get("Friend").toString());

        ////////////// Test refresh(HomeScreen) //////////////
        //// Test refreshing as Peter(Should get Janice as friend)
        // Message server
        try{
            // Read that Maria logged in
            answer = petersConnection.read();
            message = new JSONObject();
            message.put("Action", "refresh");
            petersConnection.send(message.toString());
            answer = petersConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown
        assertFalse(exceptionThrown);
        // Assert message contains all expected information and that they are not null
        assertEquals("refresh", received.get("Action").toString());
        assertNotNull(received.get("Games"));
        assertNotNull(received.get("Victories"));
        assertNotNull(received.get("ChatNames"));
        assertNotNull(received.get("Friends"));
        // Assert Janice is now a friend
        List<JSONObject> friends = (List) received.get("Friends");
        JSONObject friend = friends.get(0);
        // Janice should be in UserName1 because J(anice) is before P(eter) in the alphabet
        assertEquals("Janice", friend.get("UserName1"));
        assertEquals("Peter", friend.get("UserName2"));
        assertEquals("friends", friend.get("Status"));
        // Assert noOfGames and noOfVictories is 4
        assertEquals("4", received.get("noOfGames").toString());
        assertEquals("1", received.get("noOfVictories").toString());

        ////////////// Test game stats after a win //////////////
        // Send win-message
        try{
            message = new JSONObject();
            message.put("Action", "gamePlayed");
            message.put("Won", true);
            petersConnection.send(message.toString());
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown
        assertFalse(exceptionThrown);
        // Refresh to get new stats
        try{
            message = new JSONObject();
            message.put("Action", "refresh");
            petersConnection.send(message.toString());
            answer = petersConnection.read();
            received = server.stringToJSON(answer);
        } catch(IOException ignored){
            exceptionThrown = true;
        }
        // Assert exception was not thrown
        assertFalse(exceptionThrown);
        // Assert noOfGames is 4+1 = 5 and noOfVictories is 1+1 = 2
        assertEquals("5", received.get("noOfGames").toString());
        assertEquals("2", received.get("noOfVictories").toString());

        ////////////// Test logout //////////////
        // Send logout-message
        try{
            // Read userJoinedChat and player left game
            answer = serverConnection.read();
            message = new JSONObject();
            message.put("Action", "logout");
            serverConnection.send(message.toString());
            answer = serverConnection.read();

            // Assert answer is null
            assertEquals(null, answer);
        } catch(IOException e){
            exceptionThrown = true;
        }
        // Assert exception was not thrown
        assertFalse(exceptionThrown);
    }

    /**
     * Tests all Server-funcionality regarding JSON Web Token(JWT)
     *
     * The method createToken should return a JWT with userName, issuedAt(Date) and expiry(Date, +24hrs) as String
     * The method decodeToken should return decoded JWT as Claims or null if JWT expired
     * The method validToken should return true if expiry(Date) < issuedAt(Date)
     * The method getToken should return token if it exists, null otherwise
     */
    @Test
    public void testToken(){

        // Create and decode token
        String token = server.createToken("testName");
        Claims claims = server.decodeToken(token);

        // Get issuedAt, expiry and subject:
        long issuedAt = Long.parseLong(claims.get("iat").toString());
        long expiry = Long.parseLong(claims.get("exp").toString());
        String subject = claims.get("sub").toString();

        // Token should have been created in "the past"(some ms earlier. *1000 to convert from s to ms)
        assertTrue(System.currentTimeMillis() > issuedAt*1000);

        // Token should still be valid(created for less than 24 hours ago)
        assertTrue(System.currentTimeMillis() < expiry*1000);
        assertNotNull(server.decodeToken(token)); // Returns null if token expired

        // userName should still be 'testName'
        assertEquals("testName", subject);

        // Test expired token created by createToken()
        assertNull(server.decodeToken(EXPIREDTOKEN));

        // Test retrieving token from DB with no users:
        assertNull(server.getToken("Christian"));
    }

    /**
     * The method strInList() should return true if a List contains str, false otherwise
     */
    @Test
    public void testStrInList(){

        List<String> list = new ArrayList<>();
        // Add some names to list
        list.add("Manny");
        list.add("Jonathan");
        list.add("Lydia");

        // Assert Manny and Lydia is in list
        assertTrue(server.strInList(list, "Manny"));
        assertTrue(server.strInList(list, "Lydia"));
        // Assert Roger is not in list
        assertFalse(server.strInList(list, "Roger"));
    }

    /**
     * Creates/connects to embedded derby database
     * All rows in tables are deleted
     * @return Connection to DB
     */
    private static Connection getFreshDB(){

        try {															  // Attempt to connect to DB
            connection = DriverManager.getConnection("jdbc:derby:./LudoTestDB");
        } catch (SQLException e){										  // Connection failed
            if(e.getMessage().equals("Database './LudoTestDB' not found.")){  // DB does not exist
                try {													  // Attempt to create DB
                    connection = DriverManager.getConnection("jdbc:derby:./LudoTestDB;create=true");
                    try(Statement stmt = connection.createStatement()) {

                        // Create Users-table
                        stmt.execute("CREATE TABLE Users(" +
                                "userName varchar(20) NOT NULL," +
                                "salt blob," +
                                "password blob," +
                                "token varchar(170)," +
                                "noOfGames int," +
                                "noOfVictories int," +
                                "image blob DEFAULT null," +
                                "PRIMARY KEY(userName))");

                        // Create Chatrooms-table
                        stmt.execute("CREATE TABLE Chatrooms(" +
                                "chatName varchar(20)," +
                                "PRIMARY KEY(chatName))");

                        // Create Friendship-table
                        stmt.execute("CREATE TABLE Friendship(" +
                                "userName1 varchar(20) NOT NULL," +
                                "userName2 varchar(20) NOT NULL," +
                                "status varchar(28) NOT NULL," +
                                "PRIMARY KEY(userName1, userName2))");

                    }
                } catch (SQLException e2){								  // DB could not be created, exit
                    e.printStackTrace();
                    System.err.println("Could not create database");
                    System.exit(1);
                }
            }
            else {														  // DB exists, but could not connect
                System.err.println("Could not connect to database");
                System.exit(1);
            }
        }

        // Delete all rows:
        try(Statement stmt = connection.createStatement()){

            stmt.executeUpdate("DELETE FROM Users");
            stmt.executeUpdate("DELETE FROM Chatrooms");
            stmt.executeUpdate("DELETE FROM Friendship");

        } catch(SQLException e){
            e.printStackTrace();
            System.exit(1);
        }

        return connection;
    }
}