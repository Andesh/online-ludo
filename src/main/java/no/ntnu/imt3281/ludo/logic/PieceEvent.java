package no.ntnu.imt3281.ludo.logic;

/**
 * PieceEvent is used when a piece is moved
 * containing a Ludo object, the current active player, the piece number to be moved,
 * the position it is moved from and the position it is moved to
 */
public class PieceEvent {
    // The current ludo object
    private Ludo ludo;

    // Active player in the event
    private int activePlayer;

    // Local piece number of piece moved in event
    private int pieceNumber;

    // Local position piece is moved from in event
    private int fromPosition;

    // Local position piece is moved to in event
    private int toPosition;

    /**
     * Constructor for PieceEvent
     * @param ludo the current ludo game object
     * @param activePlayer owner of the piece to be moved
     * @param pieceNumber piece to be moved
     * @param fromPosition position to be moved from
     * @param toPosition position to be moved to
     */
    PieceEvent(Ludo ludo, int activePlayer, int pieceNumber, int fromPosition, int toPosition) {
        this.ludo = ludo;
        this.activePlayer = activePlayer;
        this.pieceNumber = pieceNumber;
        this.fromPosition = fromPosition;
        this.toPosition = toPosition;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof PieceEvent) {
            PieceEvent pieceEvent = (PieceEvent) object;
            return (ludo == pieceEvent.ludo &&
                    activePlayer == pieceEvent.activePlayer &&
                    pieceNumber == pieceEvent.pieceNumber &&
                    fromPosition == pieceEvent.fromPosition &&
                    toPosition == pieceEvent.toPosition);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public int getId() {
        return ludo.getId();
    }

    public int getActivePlayer() { return activePlayer; }

    public int getPieceNumber() {
        return pieceNumber;
    }

    public int getFromPosition() {
        return fromPosition;
    }

    public int getToPosition() {
        return toPosition;
    }
}
