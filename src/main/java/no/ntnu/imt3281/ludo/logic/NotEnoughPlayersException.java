package no.ntnu.imt3281.ludo.logic;

/**
 * En exception to be thrown when there aren't enough players in the game
 */
class NotEnoughPlayersException extends RuntimeException {
    /**
     * Throws exception
     */
    NotEnoughPlayersException() {}
}
