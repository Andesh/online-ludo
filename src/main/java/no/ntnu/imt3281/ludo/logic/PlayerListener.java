package no.ntnu.imt3281.ludo.logic;

/**
 * Listener listening for PlayerEvent
 */
public interface PlayerListener {
    /**
     * A function that every class that implements this one needs
     * @param playerEvent Player event
     */
    void playerStateChanged(PlayerEvent playerEvent);
}
