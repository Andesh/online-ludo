package no.ntnu.imt3281.ludo.logic;

/**
 * PlayerEvent is used when a player is updated
 * containing a Ludo object, and current active player and the status
 */
public class PlayerEvent {
    // static final ints used for player event status
    static final int WAITING = 1;
    static final int PLAYING = 2;
    static final int LEFTGAME = 3;
    static final int WON = 4;

    // The current ludo object
    private Ludo ludo;

    // Active player for the event
    private int activePlayer;

    // Status for the event
    private int status;

    /**
     * Constructor for PlayerEvent
     * @param ludo the current ludo game object
     * @param activePlayer the current active player
     * @param status status of the active player
     */
    PlayerEvent(Ludo ludo, int activePlayer, int status) {
        this.ludo = ludo;
        this.activePlayer = activePlayer;
        if (status == WAITING || status == PLAYING || status == LEFTGAME || status == WON) {
            this.status = status;
        }
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof PlayerEvent) {
            PlayerEvent playerEvent = (PlayerEvent) object;
            return (ludo == playerEvent.ludo &&
                    activePlayer == playerEvent.activePlayer &&
                    status == playerEvent.status);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public int getId() {
        return ludo.getId();
    }

    public int getActivePlayer() { return activePlayer; }

    public int getStatus() {
        return status;
    }
}
