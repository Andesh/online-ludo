package no.ntnu.imt3281.ludo.logic;

/**
 * Piece class holding information about a players piece
 */
public class Piece {
    // Local number for player
    private final int localPieceNumber;

    // Global number for position on board (0-15)
    private final int globalPieceNumber;

    // Local position of piece
    private int localPosition = 0;

    // Status if piece can move with current dice number (in base, blocked by tower, overshoots goal to be false)
    private boolean canMove = false;

    /**
     * Creates a new piece
     * Sets the position of the piece to 0
     * Adds piece to boardArray
     * @param ludo the current ludo object
     * @param globalPieceNumber the piece number for the board (0-15)
     * @param pieceNumber the number of the piece to the player
     */
    Piece(Ludo ludo , int globalPieceNumber, int pieceNumber) {
        this.localPieceNumber = pieceNumber;
        this.globalPieceNumber = globalPieceNumber;
        // Adds the piece to board
        ludo.positionArray[globalPieceNumber].addPiece(globalPieceNumber);
    }

    /**
     * @return the number of the piece
     */
    int getLocalPieceNumber() {
        return localPieceNumber;
    }

    /**
     * @return the global pieceNumber for board (0-15)
     */
    public int getGlobalPieceNumber() { return globalPieceNumber; }

    /**
     * @return the position of the piece
     */
    int getLocalPosition() { return localPosition; }

    /**
     * Sets new position for the piece
     * @param localPosition the new position
     */
    void setLocalPosition(int localPosition) {
        this.localPosition = localPosition;
    }

    /**
     * @return if piece can move
     */
    boolean getCanMove() {
        return canMove;
    }

    /**
     * Sets if piece can move
     * @param canMove boolean, true if piece can move
     */
    public void setCanMove(boolean canMove) {
        this.canMove = canMove;
    }
}
