package no.ntnu.imt3281.ludo.logic;

/**
 * Listener listening for a DiceEvent
 */
public interface DiceListener {
    /**
     * A function that every class that implements this one needs
     * @param event Dice event
     */
    void diceThrown(DiceEvent event);
}
