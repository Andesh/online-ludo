package no.ntnu.imt3281.ludo.logic;


/**
 * DiceEvent is used when a dice is thrown
 * containing a Ludo object, and current active player and dice number
 */
public class DiceEvent {
    // The current ludo object
    private Ludo ludo;

    // Active player used in event, player that throws dice
    private int activePlayer;

    // The dice number to be thrown
    private int diceNumber;

    /**
     * Constructor for DiceEvent
     * @param ludo the current ludo game object
     * @param activePlayer the current active player
     * @param diceNumber the current dice number
     */
    DiceEvent(Ludo ludo, int activePlayer, int diceNumber) {
        this.ludo = ludo;
        this.activePlayer = activePlayer;
        this.diceNumber = diceNumber;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof DiceEvent) {
            DiceEvent diceEvent = (DiceEvent) object;
            return (ludo == diceEvent.ludo &&
                    activePlayer == diceEvent.activePlayer &&
                    diceNumber == diceEvent.diceNumber);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Gets the id of the ludo
     * @return Id of ludo
     */
    public int getId() { return ludo.getId(); }

    /**
     * Gets the dicenumber
     * @return dicenumber
     */
    public int getDiceNumber() { return diceNumber; }
}
