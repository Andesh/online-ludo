package no.ntnu.imt3281.ludo.logic;

/**
 * En exception to be thrown when there are no room for more players in the game
 */
class NoRoomForMorePlayersException extends RuntimeException {
    /**
     * Throws exception
     */
    NoRoomForMorePlayersException() { }
}
