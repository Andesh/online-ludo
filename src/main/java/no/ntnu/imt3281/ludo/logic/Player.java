package no.ntnu.imt3281.ludo.logic;

import java.util.ArrayList;
import java.util.List;

/**
 * Player class holding information about a player in a Ludo game
 */
public class Player {
    // User name of the player
    private String playerName;

    // Color of the player
    private int playerColor;

    //State of the player (if left game, state is false)
    private boolean playerState = false;

    // List of all the players pieces
    private List<Piece> pieceList = new ArrayList<>();

    // Number of six thrown in a row each turn
    private int numberOfSixInRow = 0;

    // Number of throws in a row each turn
    private int numberOfThrowsInRow = 0;

    // Number of players pieces to reach goal (local position 59)
    private int piecesFinished = 0;

    /**
     * Creates a new player object
     * Adds four new pieces to the player
     * @param ludo the current ludo object
     * @param name the username of the new player
     * @param color the color of the new player
     */
    public Player(Ludo ludo, String name, int color) {
        this.playerName = name;
        this.playerColor = color;
        // Adds four pieces to the player
        for (int i = 0; i < 4; i++) {
            Piece piece = new Piece(ludo, color * 4 + i, i);
            pieceList.add(piece);
        }
    }

    /**
     * @return the name of the player
     */
    public String getPlayerName() {
        return playerName;
    }

    /**
     * @return the color of the player
     */
    public int getPlayerColor() {
        return playerColor;
    }

    /**
     * Sets new state of the player
     * @param playerState the new state of the player
     */
    void setPlayerState(boolean playerState) {
        this.playerState = playerState;
    }

    /**
     * @return the state of the player
     */
    boolean getPlayerState() {
        return playerState;
    }

    /**
     * Sets the player name to be displayed as Inactive
     */
    void setInactiveName() {
        playerName = "Inactive: " + playerName;
    }

    /**
     * @return the list of pieces
     */
    public List<Piece> getPieceList() {
        return pieceList;
    }

    /**
     * Gets the player piece of the given position
     * @param position the position to get the piece from
     * @return the piece number
     */
    int getPiece(int position) {
        for (Piece piece : pieceList) {
            if (piece.getLocalPosition() == position) {
                return piece.getLocalPieceNumber();
            }
        }
        // if there is no piece in given position
        return -1;
    }

    /**
     * Checks if pieces are in base/0
     * @return if all pieces is in base/0
     */
    public boolean allPiecesStartOrFinish() {
        int piecesInBaseOrFinished = 0;
        for (Piece piece : pieceList) {
            if (piece.getLocalPosition() == 0 || piece.getLocalPosition() == 59) {
                piecesInBaseOrFinished++;
            }
        }
        return piecesInBaseOrFinished == 4;
    }

    /**
     * @return number of sixes in row
     */
    public int getNumberOfSixInRow() {
        return numberOfSixInRow;
    }

    /**
     * Updates number of sixes in row
     * @param numberOfSixInRow new number of sixes in play
     */
    void setNumberOfSixInRow(int numberOfSixInRow) {
        this.numberOfSixInRow = numberOfSixInRow;
    }

    /**
     * @return number of throws in row
     */
    int getNumberOfThrowsInRow() {
        return numberOfThrowsInRow;
    }

    /**
     * Updates number of throws in row
     * @param numberOfThrowsInRow new number of throws in row
     */
    void setNumberOfThrowsInRow(int numberOfThrowsInRow) {
        this.numberOfThrowsInRow = numberOfThrowsInRow;
    }

    /**
     * @return number of pieces that is finished
     */
    int getPiecesFinished() {
        return piecesFinished;
    }

    /**
     * Updates number of players pieces that is finished
     * @param piecesFinished new number of pieces that is finished
     */
    void setPiecesFinished(int piecesFinished) {
        this.piecesFinished = piecesFinished;
    }

    /**
     * Checks if the piece can move
     * @param globalPieceNumber Int, Global piece number
     * @return Boolean, True if the piece can move
     */
    public boolean pieceCanMove(int globalPieceNumber) {
        for (Piece piece : pieceList) {
            if (piece.getGlobalPieceNumber() == globalPieceNumber ) {
                return pieceList.get(piece.getLocalPieceNumber()).getCanMove();
            }
        }
        return false;
    }

    /**
     * Gets local position of piece using its global piece number
     * @param globalPieceNumber the global piece number to use
     * @return local position of the piece
     *         -1 if not players piece
     */
    public int getLocalPosition(int globalPieceNumber) {
        for (Piece piece : pieceList) {
            if (piece.getGlobalPieceNumber() == globalPieceNumber ) {
                return piece.getLocalPosition();
            }
        }
        return -1;
    }
}
