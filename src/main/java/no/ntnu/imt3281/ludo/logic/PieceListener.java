package no.ntnu.imt3281.ludo.logic;

/**
 * Listener listening for PieceEvent
 */
public interface PieceListener {
    /**
     * A function that every class that implements this one needs
     * @param pieceEvent Piece event
     */
    void pieceMoved(PieceEvent pieceEvent);
}
