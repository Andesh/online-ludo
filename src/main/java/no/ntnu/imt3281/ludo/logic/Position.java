package no.ntnu.imt3281.ludo.logic;

import java.util.ArrayList;
import java.util.List;

/**
 * Class holding information about what pieces, player number and if it is tower
 * in current position object
 */
public class Position {
    // List of all pieces in position (can only be one pieces from same player)
    private List<Integer> pieceInPositionList = new ArrayList<>();

    // If it is tower in current position (two or more pieces)
    private boolean tower = false;

    // The the owner of the pieces in position
    private int playerNumberInPosition = -1;

    Position() {}

    /**
     * Adds piece with globalPieceNumber to pieceList
     * Updates tower status if more than one piece in position
     * @param globalPieceNumber the piece number for the global board (0-15)
     */
    void addPiece(int globalPieceNumber) {
        pieceInPositionList.add(globalPieceNumber);
        playerNumberInPosition = globalPieceNumber / 4;
        updateTowerStatus();
    }

    /**
     * Removes piece with boardPieceNumber from pieceList
     * Updates tower status
     * Sets playerNumber to -1 if no pieces in position
     * @param globalPieceNumber the piece number for the global board (0-15)
     */
    void removePiece(int globalPieceNumber) {
        int i = 0;
        // Gets what piece to remove from pieceList
        for (Integer piece : pieceInPositionList) {
            if (piece == globalPieceNumber) {
                break;
            }
            i++;
        }
        pieceInPositionList.remove(i);
        updateTowerStatus();
        // If no more pieces in position, sets player number to -1
        if (pieceInPositionList.isEmpty()) {
            playerNumberInPosition = -1;
        }
    }

    /**
     * Gets the list of pieces in position
     * Used when removing piece if other players piece lands on top
     * @return pieceInPositionList, all the pieces in position
     */
    public List getPieceInPositionList() {
        return pieceInPositionList;
    }

    /**
     * Gets the playerNumber currently in position
     * Used when removing piece if other players piece lands on top, and to check for tower
     * @return the player number owner of the piece(s) in position
     */
    public int getPlayerNumberInPosition() {
        return playerNumberInPosition;
    }

    /**
     * Sets tower to be true if more than one piece at the location
     */
    private void updateTowerStatus() {
        tower = pieceInPositionList.size() > 1;
    }

    /**
     * @return if it is a tower
     */
    boolean getTower() {
        return tower;
    }
}
