package no.ntnu.imt3281.ludo.logic;

import no.ntnu.imt3281.ludo.server.ClientConnection;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * Containing logic for ludo game
 */
public class Ludo {

    // Constants for players:
    static final int RED = 0;
    static final int BLUE = 1;
    static final int YELLOW = 2;
    static final int GREEN = 3;

    // Local position '1' to global position for all players
    private int[] firstPos = new int[] {16, 29, 42, 55};

    // Keeps track of how many active players
    private int nrOfActive = 0;

    // Keeps track of the current active player
    private int activePlayer = 0;

    // Keeps track of the last dice number thrown
    private int lastThrow = 0;

    // The id of the current game
    private int id = -1;

    // The status of the game
    private String status;

    // Holding all the players in the game
    private List<Player> playerList = new ArrayList<>();

    // Lists of listeners
    private List<DiceListener> diceListenerList = new ArrayList<>();
    private List<PieceListener> pieceListenerList = new ArrayList<>();
    private List<PlayerListener> playerListenerList = new ArrayList<>();

    /**
     * Array holding all position objects
     */
    Position[] positionArray;

    /**
     * Constructor creating a new Ludo object with new position objects
     */
    public Ludo() {
        status = "Created";

        // Creates 92 Position objects
        positionArray = new Position[91];
        for (int i = 0; i < 91; i++) {
            positionArray[i] = new Position();
        }
    }

    /**
     * The constructor of the Ludo logic. Checks if you sent more than one player, if not then throw exception
     * @param player1 String, player one
     * @param player2 String, player two
     * @param player3 String, player three
     * @param player4 String, player four
     */
    public Ludo(String player1, String player2, String player3, String player4) {

        status = "Created";

        // Creates 92 Position objects
        positionArray = new Position[91];
        for (int i = 0; i < 91; i++) {
            positionArray[i] = new Position();
        }

        // Adds players to ludo game
        addPlayer(player1);
        addPlayer(player2);
        addPlayer(player3);
        addPlayer(player4);
        
        // Adds pieces to position
        for (int i = 0; i <= nrOfPlayers() * 4; i++) {
            positionArray[i].addPiece(i);
        }

        // If not enough players, throws exception
        if (nrOfPlayers() < 2) {
            throw new NotEnoughPlayersException();
        }
    }

    /**
     * Adds the player to the game and one-ups the number of active players
     * @param userName String, username
     */
    void addPlayer(String userName) {
        if (playerList.size() < 4) {
            playerList.add(new Player(this, userName, playerList.size()));
            if (userName != null) {
                playerList.get(playerList.size() - 1).setPlayerState(true);
                nrOfActive++;
                status = "Initiated";
            }
        } else {
            throw new NoRoomForMorePlayersException();
        }
    }

    /**
     * Removes the player
     * Sets pieces back to start
     * Sets username to "Inactive: PlayerName"
     * Ends the game if only one player left in the game
     * @param userName String, username
     */
    public void removePlayer(String userName) {
        for (Player player : playerList) {
            if (player.getPlayerName() != null && player.getPlayerName().equals(userName)) {
                // Make a player event and send it to all listeners because the player left the game
                PlayerEvent playerEvent = new PlayerEvent(this, playerList.indexOf(player), PlayerEvent.LEFTGAME);
                for (PlayerListener playerListener : playerListenerList) {
                    playerListener.playerStateChanged(playerEvent);
                }
                // Sets all players pieces back to start (0)
                removeAllPieces(player);

                player.setPlayerState(false);
                nrOfActive--;
                // if the player to be removed is the active player, set next active player
                if (player.getPlayerName().equals(playerList.get(activePlayer).getPlayerName())) {
                    setNextActivePlayer();
                }
                // Sets name to 'Inactive' + playerName
                player.setInactiveName();

                if (activePlayers() == 1) {
                    wonPlayerEvent();
                }
            }
        }
    }

    /**
     * Sets all players pieces back to start
     * Used when removing a player
     * @param player the owner of the pieces
     */
    private void removeAllPieces(Player player) {
        for (Piece piece : player.getPieceList()) {
            int playerNumber = player.getPlayerColor();
            int pieceNumber = piece.getLocalPieceNumber();
            int fromPos = piece.getLocalPosition();
            // If not already in base
            if (piece.getLocalPosition() != 0) {
                // Make a piece event because the player left, so all pieces should go back to start
                // Sends event to all listeners
                PieceEvent pieceEvent = new PieceEvent(this, playerNumber, pieceNumber, fromPos, 0);
                for (PieceListener pieceListener : pieceListenerList) {
                    pieceListener.pieceMoved(pieceEvent);
                }
                // Moves piece back to original position
                moveThePiece(player,
                        piece.getLocalPieceNumber(),
                        userGridToLudoBoardGrid(player.getPlayerColor(), piece.getLocalPosition()),
                        piece.getGlobalPieceNumber(), 0);
            }
        }
    }

    /**
     * Gets the number of players in this game
     * @return Number of players in this game
     */
    int nrOfPlayers() {
        int number = 0;
        for (Player player : playerList) {
            if (player.getPlayerName() != null) {
                number++;
            }
        }
        return number;
    }

    /**
     * @return Number of active players
     */
    int activePlayers() {
        return nrOfActive;
    }

    /**
     * @return The color/number of the active player
     */
    public int activePlayer() { return activePlayer; }

    /**
     * Gets the player name of the player with given color
     * @param color The color to be searched for
     * @return The username of the player with the color
     */
    String getPlayerName(int color) {
        if (nrOfPlayers() > color) {
            return playerList.get(color).getPlayerName();
        }
        return null;
    }

    /**
     * Gets the position of a given piece of a given player
     * @param player the player number of the given player
     * @param piece the piece number of a players piece
     * @return the position of the given player
     */
    int getPosition(int player, int piece) {
        return playerList.get(player).getPieceList().get(piece).getLocalPosition();
    }

    /**
     * Creates new dice event
     * Count number of sixes thrown in a row and number of throws in a row for active player
     * Checks what pieces can move, if blocked by tower or can't make it in
     * Creates player event if next players turn
     * @param diceNumber the number on the thrown dice
     * @return the same diceNumber
     */
    public int throwDice(int diceNumber) {
        status = "Started";
        // Sets lastThrow to diceNumber to be used in movePiece
        lastThrow = diceNumber;
        // Gets current active player
        Player player = playerList.get(activePlayer);

        // DiceEvent to all DiceListeners
        DiceEvent diceEvent = new DiceEvent(this, activePlayer, diceNumber);
        for (DiceListener diceListener : diceListenerList) {
            diceListener.diceThrown(diceEvent);
        }
        // Updates number of throws in row
        player.setNumberOfThrowsInRow(player.getNumberOfThrowsInRow() + 1);

        // Sets number of six in row to zero if it is not a six
        if (diceNumber != 6) {
            player.setNumberOfSixInRow(0);
            // if any pieces are in play, count number of six in row
        } else if (!player.allPiecesStartOrFinish()) {
            player.setNumberOfSixInRow(player.getNumberOfSixInRow() + 1);
        }

        // Check what pieces can move and not
        int piecesCanMove = checkCanMove(player, diceNumber);

        // if all players pieces is at either 0 or 59 (start or finish)
        // you need six to get out and get three tries to do this
        if (player.allPiecesStartOrFinish()) {
            // if three throws in row and dice number is not 6, set to next players turn
            if (player.getNumberOfThrowsInRow() == 3 && diceNumber != 6) {
                waitingPlayerEvent();
                setNextActivePlayer();
            }
            return diceNumber;
        }

        // If three sixes in row, sets next active player
        if (player.getNumberOfSixInRow() == 3) {
            waitingPlayerEvent();
            setNextActivePlayer();
            return diceNumber;
        }

        // If no pieces can move, sets next active player
        if (piecesCanMove == 0) {
            waitingPlayerEvent();
            setNextActivePlayer();
        }
        return diceNumber;
    }

    /**
     * Generates a random int between 1 and 6
     * Throws the dice
     * @return the random int
     */
    public int throwDice() {
        Random random = new Random();
        int diceNumber = random.nextInt(6) + 1;
        // throws the number
        throwDice(diceNumber);
        return diceNumber;
    }

    /**
     * Used only in throwDice(int)
     * Checks all players pieces if they can move
     * Updates witch pieces that can move
     * @param player the owner of the pieces
     * @param diceNumber the dice number that has been thrown
     * @return number of pieces that can be moved
     */
    private int checkCanMove(Player player, int diceNumber) {
        int piecesCanMove = 0;
        for (Piece playersPiece : player.getPieceList()) {
            playersPiece.setCanMove(true);
            // If piece is in start position
            if (playersPiece.getLocalPosition() == 0) {
                if (diceNumber != 6) {
                    playersPiece.setCanMove(false);
                // If other players tower in this players position 1, can't move
                } else if (positionArray[firstPos[activePlayer]].getTower() &&
                        positionArray[firstPos[activePlayer]].getPlayerNumberInPosition() != activePlayer) {
                    playersPiece.setCanMove(false);
                } else {
                    playersPiece.setCanMove(true);
                }
            } else {
                int checkPosition = userGridToLudoBoardGrid(player.getPlayerColor(), playersPiece.getLocalPosition());
                // If piece overshoots the finish or has a tower in its path
                if ((playersPiece.getLocalPosition() + diceNumber > 59) ||
                        (checkTower(checkPosition, diceNumber))) {
                    playersPiece.setCanMove(false);
                } else {
                    playersPiece.setCanMove(true);
                }
            }
            // Counts number of pieces that can't move
            if (playersPiece.getCanMove()) {
                piecesCanMove++;
            }
        }
        return piecesCanMove;
    }

    /**
     * checks if it is a tower in the pieces path
     * @param position global position of the piece
     * @param diceNumber dice number
     * @return if it is a tower in the path
     */
    private boolean checkTower(int position, int diceNumber) {
        int i;
        for (i = position + 1; i <= position + diceNumber; i++) {
            // If tower in given position
            if (positionArray[i].getTower() && positionArray[i].getPlayerNumberInPosition() != activePlayer) {
                return true;
            }
            // Positions ends at 67, next position is 16
            if (i >= 67) {
                int diceRest = position + diceNumber - i;
                // Next position is 16
                for (int j = 16; j < 16 + diceRest; j++) {
                    if (positionArray[j].getTower() && positionArray[j].getPlayerNumberInPosition() != activePlayer) {
                        return true;
                    }
                    // If red player, should not check pos 17++
                    if (activePlayer == 0 && j == 16) {
                        break;
                    }
                }
                break;
            }
        }
        return false;
    }

    /**
     * Player event for new player to be waiting
     */
    private void waitingPlayerEvent() {
        PlayerEvent playerEvent = new PlayerEvent(this, activePlayer, PlayerEvent.WAITING);
        for (PlayerListener playerListener : playerListenerList) {
            playerListener.playerStateChanged(playerEvent);
        }
    }

    /**
     * Player event sent to all listeners that a player has won
     */
    private void wonPlayerEvent() {
        PlayerEvent playerEvent = new PlayerEvent(this, activePlayer, PlayerEvent.WON);
        for (PlayerListener playerListener : playerListenerList) {
            playerListener.playerStateChanged(playerEvent);
        }
    }

    /**
     * Moves the given piece from fromPos to toPos
     * @param playerNumber the color/number of the player
     * @param fromPos the position of the players piece
     * @param toPos the new position of the piece
     * @return true if player number is active player and if piece can move
     *         else return false
     *         or return false if player state is false (if player left or player name is null)
     *         or return false the state of the player is false
     */
    public boolean movePiece(int playerNumber, int fromPos, int toPos) {
        if (fromPos == 0) {
            toPos = 1;
        }
        if (!playerList.get(playerNumber).getPlayerState()) {
            return false;
        }
        Player player = playerList.get(playerNumber);
        int localPieceNumber = player.getPiece(fromPos);
        int fromGlobalPos = userGridToLudoBoardGrid(playerNumber, fromPos);
        int toGlobalPos = userGridToLudoBoardGrid(playerNumber, toPos);
        if (playerNumber == activePlayer && player.getPieceList().get(localPieceNumber).getCanMove()) {

            // Piece event for all piece listeners
            pieceEvent(playerNumber, localPieceNumber, fromPos, toPos);

            // If moved out of start or got other than six when there are pieces already on the board, sets next player
            if (fromPos == 0 || (lastThrow != 6 && !player.allPiecesStartOrFinish())) {
                waitingPlayerEvent();
                setNextActivePlayer();
            }

            // If it is another players piece her, remove it
            if (positionArray[toGlobalPos].getPieceInPositionList().size() == 1 && positionArray[toGlobalPos].getPlayerNumberInPosition() != playerNumber) {
                // boardPieceNumber
                int globalPieceNumberToSendBack = (int) positionArray[toGlobalPos].getPieceInPositionList().get(0);
                // playerNumber
                int playerNumberToSendBack = positionArray[toGlobalPos].getPlayerNumberInPosition();
                // player
                Player playerToSendBack = playerList.get(playerNumberToSendBack);
                // pieceNumber
                int pieceNumberToSendBack = globalPieceNumberToSendBack % 4;
                // fromPos
                int piecePositionToSendBackFrom =  playerList.get(playerNumberToSendBack).getPieceList().get(pieceNumberToSendBack).getLocalPosition();

                // Piece event to all listeners
                pieceEvent(playerNumberToSendBack, pieceNumberToSendBack, piecePositionToSendBackFrom, 0);

                // Moves the piece back to start
                // fromBoardPos = toBoardPos
                moveThePiece(playerToSendBack, pieceNumberToSendBack, toGlobalPos, globalPieceNumberToSendBack, 0);
            }

            // Moves the piece
            moveThePiece(player, localPieceNumber, fromGlobalPos, toGlobalPos, toPos);

            // Player event if all pieces for the player is finished
            pieceFinished(player, toPos);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Moves the piece from one position object to another position object
     * @param player the player that owns the piece to be moved
     * @param pieceNumber the players piece number to be moved
     * @param fromGlobalPos the global position to be moved from
     * @param toGlobalPos the global position to be moved to
     * @param toLocalPos the new local position to the piece
     */
    private void moveThePiece(Player player, int pieceNumber, int fromGlobalPos, int toGlobalPos, int toLocalPos) {
        int globalPieceNumber = player.getPlayerColor() * 4 + pieceNumber;
        positionArray[fromGlobalPos].removePiece(globalPieceNumber);
        positionArray[toGlobalPos].addPiece(globalPieceNumber);
        player.getPieceList().get(pieceNumber).setLocalPosition(toLocalPos);
    }

    /**
     * Used only in movePiece
     * Piece event to update all listeners
     * @param playerNumber the number of the owner of the piece
     * @param pieceNumber the number of the piece (local)
     * @param fromPos position to be moved from
     * @param toPos position to be moved to
     */
    private void pieceEvent(int playerNumber, int pieceNumber, int fromPos, int toPos) {
        PieceEvent pieceEvent = new PieceEvent(this, playerNumber, pieceNumber, fromPos, toPos);
        for (PieceListener pieceListener : pieceListenerList) {
            pieceListener.pieceMoved(pieceEvent);
        }
    }

    /**
     * Used only in movePiece
     * Checks if piece is finished
     * Sets inPlay to false if it is
     * If all players pieces is finished, player event for player won
     * @param player the current player that moved its piece
     * @param toPos the new position of the piece
     */
    private void pieceFinished(Player player, int toPos) {
        if (toPos == 59) {
            player.setPiecesFinished(player.getPiecesFinished() + 1);
            // Ends the game if all players pieces is finished
            if (player.getPiecesFinished() == 4) {
                status = "Finished";
                PlayerEvent playerEvent = new PlayerEvent(this, player.getPlayerColor(), PlayerEvent.WON);
                for (PlayerListener playerListener : playerListenerList) {
                    playerListener.playerStateChanged(playerEvent);
                }
            }
        }
    }

    /**
     * Sets next player to be activePlayer
     * Creates new player event to update active player for all clients
     */
    private void setNextActivePlayer() {
        Player player = playerList.get(activePlayer);
        player.setNumberOfThrowsInRow(0);
        player.setNumberOfSixInRow(0);
        activePlayer = getNextActivePlayer();
        PlayerEvent playerEvent = new PlayerEvent(this, activePlayer, PlayerEvent.PLAYING);
        for (PlayerListener playerListener : playerListenerList) {
            playerListener.playerStateChanged(playerEvent);
        }
    }

    /**
     * Gets the next player to be active player
     * @return next active players index
     */
    private int getNextActivePlayer() {
        // Goes through the next players, if state is false, next
        // If end of the players, start at beginning with next loop
        for (int i = activePlayer + 1; i < nrOfPlayers(); i++) {
            if (playerList.get(i).getPlayerState()) {
                return i;
            }
        }
        for (int i = 0; i < nrOfPlayers(); i++) {
            if (playerList.get(i).getPlayerState()) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Status of Ludo game
     * @return String 'Created' if no players,
     *                'Initiated' if at least one player,
     *                'Started' if dice has been thrown,
     *                'Finished' if one player has all pieces in 59
     */
    String getStatus() {
        return status;
    }

    /**
     * Goes through all players pieces to find which player finished
     * @return the color of the winner
     *         if no players are finished, returns -1
     */
    int getWinner() {
        // Returns Ludo.RED etc.
        for (Player player : playerList) {
            if (player.getPiecesFinished() == 4) {
                return player.getPlayerColor();
            }
        }
        return -1;
    }

    /**
     * Converts the location of pieces from user board to the ludo board
     * according to the pieces color and location
     * @param color the color or number of the given player
     * @param localPiecePosition the piece position
     * @return the position of the piece from user board to the ludo board
     */
    public int userGridToLudoBoardGrid(int color, int localPiecePosition) {
        int globalPiecePosition = (localPiecePosition + 15 + (13 * color));
        int pieceNumber = 0;
        if (!playerList.isEmpty()) {
            pieceNumber = playerList.get(color).getPiece(localPiecePosition);
        }
        // at base
        if (localPiecePosition == 0) {
            return color * 4 + pieceNumber;
        // on colored positions
        } else if (localPiecePosition > 53 && localPiecePosition < 60) {
            return localPiecePosition + 14 + (6 * color);
        // global position on board
        } else if (globalPiecePosition > 0) {
            return (globalPiecePosition <= 67) ? globalPiecePosition : (globalPiecePosition % 67) + 15;
        // if failed
        } else {
            return -1;
        }
    }

    /**
     * Adds a dice listener
     * @param diceListener Dice listener
     */
    public void addDiceListener(DiceListener diceListener) {
        diceListenerList.add(diceListener);
    }

    /**
     * Adds a piece listener
     * @param pieceListener Piece listener
     */
    public void addPieceListener(PieceListener pieceListener) { pieceListenerList.add(pieceListener); }

    /**
     * Adds a playerListener
     * @param playerListener Player listener
     */
    public void addPlayerListener(PlayerListener playerListener) { playerListenerList.add(playerListener); }

    /**
     * Removes all listeners to the client
     * @param clientConnection Client connection
     */
    public void removeListeners(ClientConnection clientConnection) {
        diceListenerList.remove(clientConnection);
        pieceListenerList.remove(clientConnection);
        playerListenerList.remove(clientConnection);
    }

    /**
     * Retrieves the position array's number i
     * @param i Int, Square to be retrieved
     * @return Int, the square
     */
    public Position getPositionArray(int i) { return positionArray[i]; }

    /**
     * Retrieves the list of players that have joined
     * @return List of players
     */
    public List<Player> getPlayerList() { return playerList; }

    /**
     * Retrieves the last throw that was made in this game
     * @return Int, The last throw
     */
    public int getLastThrow() { return lastThrow; }

    /**
     * Sets the id of this game
     * @param id Int, The id to be set
     */
    public void setId(int id) { this.id = id; }

    /**
     * Retrieves the id of this game
     * @return Int, The id of this game
     */
    public int getId() { return id; }
}
