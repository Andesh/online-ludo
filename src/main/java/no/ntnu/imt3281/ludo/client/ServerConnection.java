package no.ntnu.imt3281.ludo.client;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;


/**
 * Connection to server
 * Contains functions for server communication
 */
public class ServerConnection {

    private BufferedReader br;
    private BufferedWriter bw;
    private Socket client;

    /**
     * Constructor for Connection
     * Connects to server
     */
    public ServerConnection() {
        try{
            client = new Socket(InetAddress.getLocalHost(), 9010);

            // For receiving and sending messages:
            br = new BufferedReader(new InputStreamReader(client.getInputStream()));
            bw = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
        } catch (IOException ignored) {
            // COULD NOT CONNECT TO DB, exit
            System.exit(-1);
        }
    }

    /**
     * Sends message to server
     * @param msg String, message to be sent
     * @throws IOException if write() or flush() fails
     */
    public void send(String msg) throws IOException {
        bw.write(msg);
        bw.newLine();
        bw.flush();
    }

    /**
     * Reads message from server
     * @return String
     * @throws IOException if readline() fails
     */
    public String read() throws IOException {
        return(br.readLine());
    }

    /**
     * Closes communication-channels and socket.
     * @throws IOException if close() fails
     */
    public void close() throws IOException {
        bw.close();
        br.close();
        client.close();
    }
}