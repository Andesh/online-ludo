package no.ntnu.imt3281.ludo.client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import no.ntnu.imt3281.ludo.gui.LudoController;

import java.io.IOException;
import java.util.ResourceBundle;

/**
 * This is the main class for the client.
 * Launches application window
 */
public class Client extends Application {

    private static ResourceBundle properties = ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n");
    private LudoController ludoController;

    @Override
    public void start(Stage stage) throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("../gui/Ludo.fxml"), properties);

        BorderPane ludo = loader.load();
        ludoController = loader.getController();

        // Set scene and launch stage
        Scene scene = new Scene(ludo);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Send logout-message to server before window is closed
     */
    @Override
    public void stop() {
        ludoController.exitProgram();
        System.exit(0);
    }

    /**
     * Main, launches program
     * @param args contains the supplied command-line arguments
     */
    public static void main(String[] args) {
        launch();
    }
}
