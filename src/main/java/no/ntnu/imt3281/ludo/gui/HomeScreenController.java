package no.ntnu.imt3281.ludo.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import javafx.scene.control.cell.PropertyValueFactory;
import no.ntnu.imt3281.ludo.client.ServerConnection;

import org.json.simple.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller for the home screen for the application
 * Is used to start games, edit profile and chat
 */
public class HomeScreenController {

    // List for chat and friends
    @FXML
    private ListView<String> friendList;
    @FXML
    private ListView<String> chatList;

    // To tables for most wins and most played games
    @FXML
    private TableView<Top10Player> top10WinsTable;
    @FXML
    private TableView<Top10Player> top10GamesTable;

    // Fields for searching for chats and adding friends
    @FXML
    private TextField searchChat;
    @FXML
    private TextField searchFriend;

    // The username, games won/played
    // and feedback if things are wrong in chat/friends
    @FXML
    private Label playerName;
    @FXML
    private Label gameWins;
    @FXML
    private Label gamePlayed;
    @FXML
    private Label chatFeedback;
    @FXML
    private Label friendFeedback;


    private LudoController ludoController;
    private ServerConnection connection;
    private ResourceBundle properties;

    // List for showing in listView and tableView
    private ObservableList<Top10Player> obsTop10Games = FXCollections.observableArrayList();
    private ObservableList<Top10Player> obsTop10Victories = FXCollections.observableArrayList();
    private ObservableList<String> obsFriends = FXCollections.observableArrayList();
    private ObservableList<String> obsChatNames = FXCollections.observableArrayList();
    private ObservableList<String> obsSearch = FXCollections.observableArrayList();

    // Used for adding/extracting information to/from server-messages
    private static final String ACTION = "Action";
    private static final String GAMES = "Games";
    private static final String USERNAME = "UserName";
    private static final String CHATNAME = "ChatName";
    private static final String FRIENDSSENT = "friends.sent";
    private static final String NOOFGAMES = "noOfGames";
    private static final String NOOFVICTORIES = "noOfVictories";

    /**
     * Sets connection to server and reference to LudoController
     * Puts users userName, noOfGames and noOfVictories on screen
     * Creates columns and content for top 10-lists and puts them on screen
     * @param con ServerConnection, connection to server
     * @param controller LudoController reference(parent of this tab)
     * @param i18N ResourceBundle, used for internationalization
     * @param message JSONObject containing userName, noOfGames, noOfVictories, lists of JSONObjects containing userName and noOfGames/noOfVictories and list of ChatNames
     */
    void initialize(ServerConnection con, LudoController controller, ResourceBundle i18N, JSONObject message) {

        connection = con;
        ludoController = controller;
        properties = i18N;

        // Extract userName, noOfGames, noOfVictories from message
        String userName = message.get(USERNAME).toString();
        int noOfGames = Integer.parseInt(message.get(NOOFGAMES).toString());
        int noOfVictories = Integer.parseInt(message.get(NOOFVICTORIES).toString());

        // Put users username, noOfGames and noOfVictories on screen
        playerName.setText(userName);
        gamePlayed.setText(String.valueOf(noOfGames));
        gameWins.setText(String.valueOf(noOfVictories));

        // Initialize and put top 10 and chatName-lists, plus friends on screen
        initializeViews(message);
    }

    /**
     * Sets userName on screen
     * @param userName String, userName of user
     */
    void setUserName(String userName) {
        playerName.setText(userName);
    }

    /**
     * Opens EditProfile as tab
     */
    @FXML
    void edit() {
        ludoController.openEditTab();
    }

    /**
     * Calls logout in LudoController
     */
    @FXML
    void logout() {
        ludoController.logout();
    }

    /**
     * Calls joinRandomGame in LudoController
     */
    @FXML
    void randomGame() {
        ludoController.joinRandomGame();
    }

    /**
     * For inviting players to a game
     */
    @FXML
    void sendInvite() { /*Not implemented*/ }

    /**
     * Joins the selected chatroom from the chatlist
     */
    @FXML
    void joinChat() {
        // Empty feedback-field, extract selected chatName
        chatFeedback.setText("");
        // Gets the selected element from the chatlist
        String chatName = chatList.getSelectionModel().getSelectedItem();

        if (chatName != null && obsChatNames.contains(chatName)) {

            // Construct message
            JSONObject message = new JSONObject();
            message.put(ACTION, "joinChat");
            message.put(CHATNAME, chatName);

            // Sends message to server
            try {
                connection.send(message.toString());
            } catch(IOException ignored) {
                chatFeedback.setText(properties.getString("UnableCommunicateServer"));
            }

        } else if (obsChatNames.contains(chatName)) {
            chatFeedback.setText(properties.getString("chat.alreadyJoined"));
        } else {
            chatFeedback.setText(properties.getString("chat.emptySelection"));
        }
    }

    /**
     * Creates a new chatroom and add it to the chatlist
     * Creates a new chat with the searched for chatname
     * if the chat is not in the chatlist
     */
    @FXML
    void createChatRoom() {
        chatFeedback.setText("");

        String searchName = searchChat.getText();
        if (searchName.length() != 0) {

            // Construct message
            JSONObject message = new JSONObject();
            message.put(ACTION, "createChat");
            message.put(CHATNAME, searchName);

            // Sends message to server
            try {
                connection.send(message.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            chatFeedback.setText(properties.getString("chat.searchToCreate"));
        }
        searchChat.clear();
        // Adds the chat to the chatlist
        chatList.setItems(obsChatNames);
    }

    /**
     * Adds a ChatName to list of ChatRooms
     * @param message JSONObject containging 'ChatName'
     */
    void addChatRoomToList(JSONObject message) {
        String chatName = message.get(CHATNAME).toString();
        obsChatNames.add(chatName);
        chatList.setItems(obsChatNames);
    }

    /**
     * Shows that creating chatroom fails
     */
    void createChatRoomFail() {
        chatFeedback.setText(properties.getString("couldNotCreateChatRoom"));
    }

    /**
     * Searches for the chat in the chatlist
     * @param searchName The chatname the user searches for
     */
    private void searchChat(String searchName) {
        obsSearch.clear();

        for(String name : obsChatNames) {
            // If the chatlist has the name the user searches for
            if (name.contains(searchName)) {
                // Adds the element to a new list
                obsSearch.add(name);
            }
        }

        if(obsSearch.isEmpty()) {
            obsSearch.add("No chatroom containing "+searchName);
        }
        // Sets the elements to the list
        chatList.setItems(obsSearch);
    }

    /**
     * Sends friend-invite to server
     */
    @FXML
    void addFriend(ActionEvent event) {
        String userToInvite = searchFriend.getText();
        friendFeedback.setText("");

        if(!userToInvite.isEmpty()) {
            // Construct message
            JSONObject message = new JSONObject();
            message.put(ACTION, "friendRequest");
            message.put("UserToInvite", userToInvite);

            // Send message to server
            try {
                connection.send(message.toString());
            } catch(IOException ignored) {}
        } else {
            friendFeedback.setText(properties.getString("friend.emptyField"));
        }
        searchFriend.clear();
    }

    /**
     * Accepts friendinvites from other users
     */
    @FXML
    void acceptRequest(ActionEvent event) {
        // Gets the name from the friendlist
        String friend = friendList.getSelectionModel().getSelectedItem();
        friendFeedback.setText("");

        if(friend!= null && !friend.isEmpty() && friend.contains("(received)")) {
            String[] splitStr = friend.split("\\s+");
            friend = splitStr[0];

            // Construct message
            JSONObject message = new JSONObject();
            message.put(ACTION, "friendAccept");
            message.put(USERNAME, friend);

            // Send message to server
            try {
                connection.send(message.toString());
            } catch(IOException ignored) {}

        } else {
            friendFeedback.setText(properties.getString("friend.noAccept"));
        }
    }

    /**
     * Tells user that friend-request was sent, that invited user does not exist,
     * already friends, already invited or an error occurred
     * @param message JSONObject containing feedback
     */
    void friendRequestFeedback(JSONObject message) {
        friendFeedback.setText("");
        String msg = message.get("Message").toString();
        String action = message.get(ACTION).toString();

        friendFeedback.setText(properties.getString(msg));

        if("friendRequestOK".equals(action)) {
            String userName = message.get(USERNAME).toString();
            obsFriends.add(userName + " " + properties.getString(FRIENDSSENT));
            friendList.setItems(obsFriends);
        }
    }

    /**
     * Adds a new friend to ListView on screen
     * @param message JSONObject containing 'Friend'
     */
    void addFriendToList(JSONObject message) {
        String friend = message.get(USERNAME).toString();

        // Remove friend with sent/received
        obsFriends.remove(friend + " " + properties.getString(FRIENDSSENT));
        obsFriends.remove(friend + " " + properties.getString("friends.received"));

        // Add friend without sent/received
        obsFriends.add(friend);
    }

    /**
     * Updates the chatlist when the searchfield is empty
     */
    @FXML
    void updateList() {
        String searchName = searchChat.getText();

        // When the searchfield is empty
        if (searchName.length() == 0) {
            // "Resets" the chatlist
            chatList.setItems(obsChatNames);
            chatFeedback.setText("");
        } else {
            searchChat(searchName);
        }
    }

    /**
     * Messages server for refresh of top 10-lists and available ChatRooms
     */
    @FXML
    void requestRefresh() {
        JSONObject message = new JSONObject();
        message.put(ACTION, "refresh");

        // Send message
        try {
            connection.send(message.toString());
        } catch(IOException ignored) {}
    }

    /**
     * Updates top 10-lists and available ChatRooms
     * @param message JSONObject containing 'Games', 'Victories', 'ChatNames', 'noOfGames' and 'noOfVictories'
     */
    void refresh(JSONObject message) {

        // Extract top 10 lists, list of chatNames and noOf from message
        List<JSONObject> top10Games = (List) message.get(GAMES);
        List<JSONObject> top10Victories = (List) message.get("Victories");
        List<JSONObject> friends = (List<JSONObject>) message.get("Friends");
        List<String> chatNames = (List) message.get("ChatNames");
        String noOfGames = message.get(NOOFGAMES).toString();
        String noOfVictories = message.get(NOOFVICTORIES).toString();

        // Put lists on screen
        setViews(top10Games, top10Victories, friends, chatNames);

        // Update labels
        gamePlayed.setText(noOfGames);
        gameWins.setText(noOfVictories);
    }

    /**
     * Initializes TableViews and ListViews, and inserts ifnormation
     * @param message JSONObject containing Lists 'Games', 'Victories', 'Friends' and 'ChatNames'
     */
    private void initializeViews(JSONObject message) {

        // Extract top 10 lists, friends and ChatNames from message
        List<JSONObject> top10Games = (List) message.get(GAMES);
        List<JSONObject> top10Victories = (List) message.get("Victories");
        List<JSONObject> friends = (List<JSONObject>) message.get("Friends");
        List<String> chatNames = (List) message.get("ChatNames");

        // Create columns and set values for top10 games played
        TableColumn<Top10Player, String> playerMatchName = new TableColumn<>("Username");
        TableColumn<Top10Player, String> playerMatches = new TableColumn<>(GAMES);

        playerMatchName.setCellValueFactory(new PropertyValueFactory<>("userName"));
        playerMatches.setCellValueFactory(new PropertyValueFactory<>("noOf"));
        playerMatchName.setMinWidth(100);

        // Create columns and set values for top10 wins
        TableColumn<Top10Player, String> playerWinsName = new TableColumn<>("Username");
        TableColumn<Top10Player, String> playerWins= new TableColumn<>("Wins");
        playerWinsName.setMinWidth(113);

        playerWinsName.setCellValueFactory(new PropertyValueFactory<>("userName"));
        playerWins.setCellValueFactory(new PropertyValueFactory<>("noOf"));

        // Put lists on screen
        setViews(top10Games, top10Victories, friends, chatNames);

        // Add columns to top 10 TableViews
        top10GamesTable.getColumns().addAll(playerMatchName, playerMatches);
        top10WinsTable.getColumns().addAll(playerWinsName, playerWins);
    }

    /**
     * Converts Lists to ObservableLists and inserts them into Table-/ListViews
     * @param top10Games List of JSONObjects containing UserName and noOfGames
     * @param top10Victories List of JSONObjects containing UserName and noOfVictories
     * @param friends List of JSONObjects containing UserName1, UserName2 and status
     * @param chatNames List containing ChatNames as String
     */
    private void setViews(List top10Games, List top10Victories, List<JSONObject> friends, List chatNames) {

        // Convert to ObservableLists
        obsTop10Games = jsonListToObservableList(top10Games, true);
        obsTop10Victories = jsonListToObservableList(top10Victories, false);
        if(chatNames != null) {
            obsChatNames = FXCollections.observableList(chatNames);
        }
        if(friends != null) {

            List<String> list = new ArrayList<>();
            String ownName = playerName.getText();

            for(JSONObject friend : friends) {

                // Extract userNames and status of friendship
                String userName1 = friend.get("UserName1").toString();
                String userName2 = friend.get("UserName2").toString();
                String status = friend.get("Status").toString();

                // Set name of friend(not equal to own name)
                String name = (userName1.equals(ownName))? userName2 : userName1;

                // If they are friends
               if("friends".equals(status)) {
                   list.add(name);
               // If user has received friend request
               } else if(status.contains(ownName)) {
                   list.add(name + " " + properties.getString("friends.received"));
               // If user has sent friend request
               } else{
                   list.add(name + " " + properties.getString(FRIENDSSENT));
               }
            }
            obsFriends = FXCollections.observableList(list);
        }

        // Insert into Views
        top10GamesTable.setItems(obsTop10Games);
        top10WinsTable.setItems(obsTop10Victories);
        friendList.setItems(obsFriends);
        chatList.setItems(obsChatNames);
    }

    /**
     * Converts list of JSONObjects to ObservableList
     * @param top10 List of JSONObjects: containing UserName and noOfGames/noOfVictories
     * @param top10Games Boolean, true if list contains noOfGames, false if list contains noOfVictories
     * @return ObservableList with Top10Player-objects
     */
    private ObservableList<Top10Player> jsonListToObservableList(List<JSONObject> top10, boolean top10Games) {

        ObservableList<Top10Player> list = FXCollections.observableArrayList();
        String userName;
        int noOf;

        // For every player, extract userName and noOfGames or noOfVictories
        for(JSONObject player : top10) {
            userName = player.get(USERNAME).toString();
            if(top10Games) {
                noOf = Integer.parseInt(player.get(NOOFGAMES).toString());

            } else {
                noOf = Integer.parseInt(player.get(NOOFVICTORIES).toString());
            }
            // Add new top10Player-object to list
            list.add(new Top10Player(userName, noOf));
        }
        return list;
    }
}