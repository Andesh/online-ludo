package no.ntnu.imt3281.ludo.gui;

/**
 * Holds a players userName and an int(noOfGames or noOfVictories)
 * Used by a column in ObservableLists
 */
public class Top10Player {

    private String userName;
    private int noOf;

    /**
     * Constructor, sets userName and noOf
     * @param userName String, userName of player
     * @param noOf int, noOfGames or noOfVictories of player
     */
    Top10Player(String userName, int noOf) {

        this.userName = userName;
        this.noOf = noOf;
    }

    /**
     * Gets userName
     * @return String, userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Gets noOf
     * @return int, noOf
     */
    public int getNoOf() {
        return noOf;
    }
}