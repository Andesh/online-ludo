package no.ntnu.imt3281.ludo.gui;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import no.ntnu.imt3281.ludo.client.ServerConnection;
import no.ntnu.imt3281.ludo.logic.Ludo;
import no.ntnu.imt3281.ludo.logic.Piece;
import org.json.simple.JSONObject;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller for a game
 */
public class GameBoardController {

    @FXML
    private Pane pieces;

    // The name of the players
    @FXML
    private Label player1Name;
    @FXML
    private Label player2Name;
    @FXML
    private Label player3Name;
    @FXML
    private Label player4Name;

    // Images for active players
    @FXML
    private ImageView player1Active;
    @FXML
    private ImageView player2Active;
    @FXML
    private ImageView player3Active;
    @FXML
    private ImageView player4Active;

    // The dice
    @FXML
    private ImageView diceThrown;

    // The dice button
    @FXML
    private Button diceButton;

    // Countdown to the game starts
    @FXML
    private Label countDownLabel;
    @FXML
    private Label countDownNr;

    // The area that the gamechat is shown
    @FXML
    private TextArea chatArea;

    // Field you can write chat in
    @FXML
    private TextField textToSay;

    // Timer is used as a countdown before a game starts
    private int timer;
    // ID of the game
    private int id;
    // Number of players joined
    private int nrOfJoinedPlayers;

    // Size of the pieces, and the offset to center them in the square
    private int pieceSize;
    // Offset from left corner so that each piece is centered in their square
    private float pieceOffset;
    // Length to the next square / length of each square
    private float boardOffset;

    // X and Y of each grids top left corner
    private int[] gridX;
    private int[] gridY;
    private int[] finishes;

    // Lists of GUI-artifacts for easier editing
    private ArrayList<Circle> circles = new ArrayList<>();
    private ArrayList<String> players = new ArrayList<>();
    private ArrayList<Color> colors = new ArrayList<>();
    private ArrayList<ImageView> activeImages = new ArrayList<>();
    private ArrayList<Label> nameLabels = new ArrayList<>();

    // The username of the owner of this controller
    private String userName;
    // Whether the game har started or not
    private boolean started;
    // Can the user throw the dice?
    private boolean canThrow;
    // If you are the reason for this pieceEvent, then ignore message from server
    private boolean ignorePieceEvent;
    // Whether you have won or not when the game ends
    private boolean won;
    // If user has closed tab
    private boolean closed;

    // Used for client/server communication:
    private static final String ACTION = "Action";
    private static final String GAMEID = "GameID";
    private static final String CHATJOINED = "chat.joined";

    // The connection to server
    private ServerConnection connection;
    // Local ludo/logic
    private Ludo ludo;
    // Controller of the main application. Used for closing this game
    private LudoController ludoController;

    // I18N-file
    private ResourceBundle properties;

    // Timestamp the messages in chat
    private SimpleDateFormat time = new SimpleDateFormat("HH:mm");

    /**
     * Sets connection to server
     * @param con Serverconnection
     * @param controller Ludocontroller
     * @param userName String, username
     */
    void initialize(ServerConnection con, LudoController controller, String userName){
        connection = con;
        this.userName = userName;
        properties = ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n");
        ludoController = controller;

        started = false;
        // Set all top-left corners of every box/square, in pixel-position
        gridX = new int[]{554, 602, 506, 554, 554, 602, 506, 554, 122, 74,  170, 122, 122, 74,  170, 122, 386, 386, 386, 386, 386, 434, 482, 530, 578, 626, 674, 674, 674, 626, 578, 530, 482, 434, 386, 386, 386, 386, 386, 386, 338, 290, 290, 290, 290, 290, 290, 242, 194, 146, 98,  50,  2,   2,   2,   50,  98,  146, 194, 242, 290, 290, 290, 290, 290, 290, 338, 386, 338, 338, 338, 338, 338, 338, 626, 578, 530, 482, 434, 386, 338, 338, 338, 338, 338, 338, 50,  98,  146, 194, 242, 290};
        gridY = new int[]{74, 122, 122, 170, 506, 554, 554, 602, 506, 554, 554, 602, 74, 122, 122, 170, 50, 98, 146, 194, 242, 290, 290, 290, 290, 290, 290, 338, 386, 386, 386, 386, 386, 386, 434, 482, 530, 578, 626, 674, 674, 674, 626, 578, 530, 482, 434, 386, 386, 386, 386, 386, 386, 338, 290, 290, 290, 290, 290, 290, 242, 194, 146, 98, 50, 2, 2, 2, 50, 98, 146, 194, 242, 290, 338, 338, 338, 338, 338, 338, 626, 578, 530, 482, 434, 386, 338, 338, 338, 338, 338, 338,};
        finishes = new int[]{73, 79, 85, 91};

        pieceSize = 20;
        // PieceOffset is used to push the piece a little away from the corner, so it is centered
        pieceOffset = 2.5f;
        // The length of every square
        boardOffset = 48;
        canThrow = true;
        ignorePieceEvent = false;
        closed = false;

        // No players have joined
        players.add(null);
        players.add(null);
        players.add(null);
        players.add(null);

        // Add different colors for the pieces
        colors.add(Color.ORANGERED);
        colors.add(Color.ROYALBLUE);
        colors.add(Color.LIGHTGOLDENRODYELLOW);
        colors.add(Color.FORESTGREEN);

        // Store the dice that shows which player is active, for later use for every playerEvent
        activeImages.add(player1Active);
        activeImages.add(player2Active);
        activeImages.add(player3Active);
        activeImages.add(player4Active);

        // Store the names of every player for later setting them, and editing them
        nameLabels.add(player1Name);
        nameLabels.add(player2Name);
        nameLabels.add(player3Name);
        nameLabels.add(player4Name);

        // sendChat when enter-key pressed in inputChat-field
        textToSay.setOnKeyPressed(keyEvent -> {
            if(keyEvent.getCode() == KeyCode.ENTER){
                sendChat();
            }
        });
    }

    /**
     * Runs when user clicks on the board.
     * If the user isn't the active player, all this code will be ignored.
     * If the user is active player and they have thrown the dice, then do the correct commands
     * @param event Event, Mouseclick on the actual board
     */
    @FXML
    void boardClicked(MouseEvent event) {
        if (started && players.get(ludo.activePlayer()).equals(userName)) {
            double x;
            double y;

            // Find global pos of mouseclick
            for (int i = 0; i < 91; i++) {
                x = event.getX();
                y = event.getY();

                // Find correct square
                if (x > gridX[i] && x < gridX[i] + boardOffset && y > gridY[i] && y < gridY[i] + boardOffset) {

                    int playerInPos = ludo.getPositionArray(i).getPlayerNumberInPosition();
                    // If there is a piece here, and is it "mine"
                    if (playerInPos != -1 && ludo.getPlayerList().get(playerInPos).getPlayerName().equals(userName)
                            && playerInPos == ludo.activePlayer()) {

                        int globalPieceNumber = (int) ludo.getPositionArray(i).getPieceInPositionList().get(0);

                        // Checks if the owner of the piece in the position is active player
                        if (ludo.getPositionArray(i).getPlayerNumberInPosition() == ludo.activePlayer() &&
                                // Checks if piece can move (if tower blocks or dice number to large to move into finish)
                                ludo.getPlayerList().get(ludo.activePlayer()).pieceCanMove(globalPieceNumber)) {

                            int localPosition = ludo.getPlayerList().get(ludo.activePlayer()).getLocalPosition(globalPieceNumber);

                            // Piece does not overshoot finish
                            if (i + ludo.getLastThrow() <= finishes[ludo.activePlayer()]) {
                                sendMovePiece(localPosition);
                            }
                        }
                    }
                    break;
                }
            }
        }
    }

    /**
     * Sends a game-chat message from user to the server.
     */
    @FXML
    void sendChat() {
        // Construct login-message
        JSONObject message = new JSONObject();
        message.put(ACTION, "gameChat");
        message.put(GAMEID, getId());
        message.put("Message", textToSay.getText());

        // Send message to server
        try {
            connection.send(message.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        textToSay.setText("");
    }

    /**
     * If the user is active user then send message to server to throw the dice
     * @param event Event, when user pushes button. Is never used
     */
    @FXML
    void throwDiceButton(ActionEvent event) {
        // Is it your turn && Are you allowed to throw
        if (started && players.get(ludo.activePlayer()).equals(userName) && canThrow) {
            diceThrown.setVisible(false);

            // Construct throwDice-message
            JSONObject message = new JSONObject();
            message.put(ACTION, "throwDice");
            message.put(GAMEID, getId());

            // Send message to server
            try {
                connection.send(message.toString());
            } catch (IOException e) {}
            // You have now thrown the dice, and cannot throw again. This is updated in a diceEvent (Code bellow)
            canThrow = false;
        }
    }

    /**
     * Message from server/diceEvent that someone threw the dice
     * @param diceNumber int, Dicenumber randomly generated by the server
     */
    void throwDice(int diceNumber) {
        // Throw the dice locally with the number from server
        ludo.throwDice(diceNumber);

        Image image = null;
        // This is supposed to show the rolldice image, but is currently not working
        Image image2;
        image2 = new Image(GameBoardController.class.getResourceAsStream("/images/rolldice.png"));

        diceThrown.setImage(image2);
        diceThrown.setVisible(true);

        // Sets the correct image according to what dice number the server got
        switch (diceNumber) {
            case 1:
                image = new Image(GameBoardController.class.getResourceAsStream("/images/dice1.png"));
                break;
            case 2:
                image = new Image(GameBoardController.class.getResourceAsStream("/images/dice2.png"));
                break;
            case 3:
                image = new Image(GameBoardController.class.getResourceAsStream("/images/dice3.png"));
                break;
            case 4:
                image = new Image(GameBoardController.class.getResourceAsStream("/images/dice4.png"));
                break;
            case 5:
                image = new Image(GameBoardController.class.getResourceAsStream("/images/dice5.png"));
                break;
            case 6:
                image = new Image(GameBoardController.class.getResourceAsStream("/images/dice6.png"));
                break;
            default:
                break;
        }
        diceThrown.setImage(image);

        // User can now throw again. Something to think about - this does so that if a six is thrown you can just throw again without moving
        //// but we thought that would add positively to the game, if you spam the throw dice, it is your mistake
        if (ludo.getPlayerList().get(ludo.activePlayer()).allPiecesStartOrFinish()) {
            canThrow = true;
        }
    }

    /**
     * Updates the player to the correct status when a playerEvent occurs
     * @param activePlayer Int, The player whom the state change is about
     * @param status Int, Which status they are to be changed to
     */
    void playerStateChange(int activePlayer, int status) {
        switch (status) {
            // Player is done with their turn, they are now WAITING
            case 1:
                activeImages.get(activePlayer).setVisible(false);
                break;
            // It is now the players turn, they are now PLAYING
            case 2:
                activeImages.get(activePlayer).setVisible(true);
                canThrow = true;
                break;
            // Player left, they are now LEFTGAME
            case 3:
                nameLabels.get(activePlayer).setText("Inactive:\n" + ludo.getPlayerList().get(activePlayer).getPlayerName());
                activeImages.get(activePlayer).setVisible(false);
                ludo.removePlayer(ludo.getPlayerList().get(activePlayer).getPlayerName());
                break;
            // Player won, they are now WON
            case 4:
                Image image = new Image(GameBoardController.class.getResourceAsStream("/images/won.png"));
                activeImages.get(activePlayer).setImage(image);
                chatArea.appendText(players.get(activePlayer) + " " + properties.getString("chat.gameWon"));

                // If someone has won, update the games played and games won
                if (players.get(activePlayer).equals(userName)) {
                    won = true;
                }
                // Construct GamePlayed-message
                JSONObject message = new JSONObject();
                message.put(ACTION, "gamePlayed");
                message.put("Won", won);

                // Send message to server
                try {
                    connection.send(message.toString());
                } catch (IOException e) {}

                // Disable the button, and effectively stop the game, because someone has won!
                diceButton.setDisable(true);
                break;
            default:
                break;
        }
    }

    /**
     * Moves a piece according to the pieceEvent from server
     * @param activePlayer Int, the player who owns the piece to be moved
     * @param fromPos Int, The position the piece should move from
     * @param toPos Int, The position the piece should move to
     * @param pieceNumber Int, The local piece number
     */
    void movePiece(int activePlayer, int fromPos, int toPos, int pieceNumber) {
        // Do not do all of this unless it wasn't you who moved the piece
        if (!ignorePieceEvent) {
            int globalToPos = ludo.userGridToLudoBoardGrid(activePlayer, toPos);
            int globalPieceNumber = ludo.getPlayerList().get(activePlayer).getPieceList().get(pieceNumber).getGlobalPieceNumber();
            // Offset multiplying factor
            int nrOfPieces = ludo.getPositionArray(globalToPos).getPieceInPositionList().size();

            // Check if there is a playerPiece here, if so send it back to their home, then continue moving "myself"
            if (ludo.getPositionArray(globalToPos).getPlayerNumberInPosition() != -1) {
                // Find out which player the piece belongs to
                int otherPieceNumber = (int) ludo.getPositionArray(globalToPos).getPieceInPositionList().get(0);
                int player = otherPieceNumber / 4;

                // If this piece is mine, make tower, else send opponent's piece home
                if (player == activePlayer) {
                    movePiece(circles.get(globalPieceNumber), globalToPos, -5 * nrOfPieces);
                    movesThePiece(activePlayer, fromPos, toPos);
                } else {
                    // Move opponents piece home. Move my piece to position. Then tell ludo to do the same
                    movePiece(circles.get(otherPieceNumber), otherPieceNumber, 0);
                    movePiece(circles.get(globalPieceNumber), globalToPos, 0);
                    movesThePiece(activePlayer, fromPos, toPos);
                    ignorePieceEvent = true;
                }
                return;
            }
            // If the was no piece here, move my piece to position, peacefully
            movePiece(circles.get(globalPieceNumber), globalToPos, 0);
            movesThePiece(activePlayer, fromPos, toPos);
        } else {
            ignorePieceEvent = false;
        }
    }

    /**
     * Moves the piece logically and updates whether the user can move pieces and throw again or not
     * @param activePlayer Int, The active player
     * @param fromPos Int, The position to move from
     * @param toPos Int, The position to move to
     */
    private void movesThePiece(int activePlayer, int fromPos, int toPos) {
        // Move the piece logically
        ludo.movePiece(activePlayer, fromPos, toPos);

        // Needed for GUI, to prevent user from moving pieces
        for (Piece piece : ludo.getPlayerList().get(activePlayer).getPieceList()) {
            piece.setCanMove(false);
        }

        // If user threw 6 and total throws with a six is less than 3, you may throw again
        if (ludo.getLastThrow() == 6 && ludo.getPlayerList().get(activePlayer).getNumberOfSixInRow() < 3) {
            canThrow = true;
        }
    }

    /**
     * Sends message to server about what piece the user wants to move
     * @param localPosition Int, Position to move from | Also used to know which piece to move
     */
    private void sendMovePiece(int localPosition) {
        // Tell server to move the piece
        JSONObject message = new JSONObject();
        message.put(ACTION, "movePiece");
        message.put(GAMEID, getId());
        message.put("ActivePlayer", ludo.getPlayerList().get(ludo.activePlayer()).getPlayerColor());
        message.put("LocalPos", localPosition);
        message.put("LocalPosTo", localPosition + ludo.getLastThrow());

        // Send message to server
        try {
            connection.send(message.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Whenever a player joins a game the existing names will be set, and the current time of the countdown will be displayed
     * @param message Message received from server
     */
    void startWait(JSONObject message) {

        id = Integer.parseInt(message.get(GAMEID).toString());

        // Nobody can play the game | No active player
        for (int i = 0; i < 4; i++) {
            nameLabels.get(i).setText("");
            activeImages.get(i).setVisible(false);
        }

        diceThrown.setVisible(false);

        // Set the names
        updateUsers(message);

        // Set countdown
        startTimer(Integer.parseInt(message.get("Timer").toString()));
    }

    /**
     * Update the game's joined users
     * @param message JSONObject containing, among other things, a list of players in this game
     */
    void updateUsers(JSONObject message) {
        List list = (List) message.get("PlayersJoined");
        nrOfJoinedPlayers = list.size();

        // Tell users a player joined
        chatArea.appendText("[" + time.format(new Date()) + "]: " + list.get(nrOfJoinedPlayers - 1).toString() + " " + properties.getString(CHATJOINED) + "\n");

        // If it was you who joined - add all the joined players ELSE add only the last joined players
        if (list.get(nrOfJoinedPlayers - 1).equals(userName)) {
            for (int i = 0; i < nrOfJoinedPlayers; i++) {
                nameLabels.get(i).setText(list.get(i).toString());
                players.remove(i);
                players.add(i, list.get(i).toString());
            }
        } else {
            nameLabels.get(nrOfJoinedPlayers-1).setText(list.get(nrOfJoinedPlayers-1).toString());
            players.remove(nrOfJoinedPlayers-1);
            players.add(nrOfJoinedPlayers-1, list.get(nrOfJoinedPlayers-1).toString());
        }
        // If there is 4 players joined, you can start the game!
        if (nrOfJoinedPlayers == 4 && !closed){
            startGame();
        }
    }

    /**
     * Start a countdown from the given time and displays it in the gameboard
     * @param time int, The time remaining on the countdown
     */
    private void startTimer(int time) {
        timer = time;
        // Counts down in GUI and updates the timer
        Runnable runTimer = () -> {
            while(timer <= 60 && !started) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ignored) {
                }
                timer++;
                Platform.runLater(this::setAndCheckTimer);
            }
        };
        Thread thread = new Thread(runTimer);
        thread.start();
    }

    /**
     * Updates the timer and checks if it has reached 0
     * Depending on number of joined players,
     * it will start or exit the game
     */
    private void setAndCheckTimer() {
        if (60 - timer <= 0) {
            // If there are enough players and the game hasn't started; start the game
            //// else; there are not enough players so exit the game. The notEnou.. check is not "necessary", but fixes multiple calls of leaveGame
            if (nrOfJoinedPlayers > 1 && !started && !closed) {
                startGame();
            } else if (nrOfJoinedPlayers == 1 && !started) {
                nrOfJoinedPlayers = 0;
                ludoController.leaveGame(this.getId(), true);
            }
        } else {
            countDownNr.setText(Integer.toString(60 - timer));
        }
    }

    /**
     * Sets a message in the chatarea
     * @param chatMessage String message to go in chat
     */
    void setChatText(String chatMessage) {
        chatArea.appendText("[" + time.format(new Date()) + "]: " + chatMessage + "\n");
    }

    /**
     * Starts the game
     */
    private void startGame() {
        started = true;
        setChatText(properties.getString("chat.startGame"));
        player1Active.setVisible(true);
        countDownNr.setText("");
        countDownLabel.setText("");

        // Construct startGame-message
        JSONObject message = new JSONObject();
        message.put(ACTION, "startGame");
        message.put(GAMEID, getId());
        message.put("Players", players);

        // Send message to server
        try {
            connection.send(message.toString());
        } catch (IOException e) {}

        ludo = new Ludo(players.get(0), players.get(1), players.get(2), players.get(3));
        ludo.setId(getId());
        makePieces();
    }

    /**
     * Makes pieces for the game
     * For each player: adds four pieces and sets their pos
     */
    private void makePieces() {
        int nrOfPieces = 0;
        int square = 0;

        for (int i = 0; i < 4; i++) {
            while (nrOfPieces != 4) {
                nrOfPieces++;
                // Make a new circle
                Circle piece = new Circle(pieceSize, colors.get(i));
                piece.setStroke(Color.BLACK);
                // Add the piece to the GUI list of pieces aka. circles
                circles.add(piece);
                // Move the piece tho its home position
                movePiece(piece, square++, 0);
                // Shows only the pieces of players who has joined the game
                if (players.get(i) != null) {
                    pieces.getChildren().addAll(piece);
                }
            }
            nrOfPieces = 0;
        }
    }

    /**
     * Moves a piece to the given square
     * Makes tower if necessary
     * @param piece Circle, the piece you want to move
     * @param square int, the global square-number
     * @param offset int, offset that is used to make towers
     */
    private void movePiece(Circle piece, int square, int offset) {
        piece.relocate(gridX[square] + pieceOffset, gridY[square] + pieceOffset + offset);
    }

    /**
     * Removes a player before the game has started
     * @param message JSONObject containing 'UserName'
     */
    void playerLeft(JSONObject message){

        // Extract userName
        String name = message.get("UserName").toString();

        // Get index of player and remove from list and labels
        int index = players.indexOf(name);
        players.set(index, null);
        nameLabels.get(index).setText("");
        nrOfJoinedPlayers--;

        switch(index){
            case 0: player1Name.setText("");
                    break;
            case 1: player2Name.setText("");
                    break;
            case 2: player3Name.setText("");
                break;
            case 3: player4Name.setText("");
                break;
            default:
                break;
        }
        chatArea.appendText("[" + time.format(new Date()) + "]: " + userName + " " + properties.getString("chat.left") + "\n");
    }

    /**
     * Updates boolean closed to true
     */
    void setClosed(){
        closed = true;
    }

    /**
     * Gets the local gameID variable
     * @return int, id of the game
     */
    int getId() { return id; }

    /**
     * Sets the lokal gameID variable
     * @param gameID, string, id of the game
     */
    public void setId(String gameID) { id = Integer.parseInt(gameID); }
}