package no.ntnu.imt3281.ludo.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import no.ntnu.imt3281.ludo.client.ServerConnection;
import org.json.simple.JSONObject;

import java.io.*;
import java.util.Base64;
import java.util.ResourceBundle;

/**
 * Controller for edit profile
 * Can change username, password and profile picture
 */
public class EditProfileController {

    // Fields for changing username and password
    @FXML
    private TextField newUsername;
    @FXML
    private TextField oldPwd;
    @FXML
    private TextField repeatPwd;
    @FXML
    private TextField newPwd;

    // Labels for username and fieldnames
    @FXML
    private Label userName;
    @FXML
    private Label changePasswordFeedback;
    @FXML
    private Label changeNameFeedback;
    @FXML
    private Label changeImageFeedback;

    // Imageview to show profileimage
    @FXML
    private ImageView imageView;

    // Connection to server
    private ServerConnection connection;
    // Reference to LudoController who has this tab in its tabbedPane
    private LudoController ludoController;
    // I18N-file
    private ResourceBundle properties;

    // Used for client/server communication:
    private static final String ACTION = "Action";

    /**
     * Initialize
     * @param con Serverconnection, connection to server
     * @param controller LudoController, reference to controller of this tab
     * @param i18N ResourceBundle, used for internationalization
     */
    void initialize(ServerConnection con, LudoController controller, ResourceBundle i18N){

        connection = con;
        ludoController = controller;
        properties = i18N;
    }

    /**
     * Tries to update userName and/or password
     * @param event Button
     */
    @FXML
    void save(ActionEvent event) {

        // Clear feedback-fields:
        changeNameFeedback.setText("");
        changePasswordFeedback.setText("");
        changeImageFeedback.setText("");

        //// Check if user wants to change username
        // Extract userName
        String newUserName = newUsername.getText();

        // If userName-field was not empty
        if(!newUserName.isEmpty()){

            if(ludoController.isValid(newUserName, false)){

                // Construct message
                JSONObject message = new JSONObject();
                message.put(ACTION, "change username");
                message.put("UserName", userName.getText());
                message.put("NewUserName", newUserName);

                // Send message
                try {
                    connection.send(message.toString());
                } catch(IOException ignored){}
            } else{
                changeNameFeedback.setText(properties.getString("nameChangeInvalid"));
            }
        }

        //// Check if user wants to change password
        // Extract old password, new password and repeated new password
        String oldPassword = oldPwd.getText();
        String newPassword = newPwd.getText();
        String repeatedPwd = repeatPwd.getText();

        // If all password-fields are not empty
        if(!oldPassword.isEmpty() && !newPassword.isEmpty() && !repeatedPwd.isEmpty()){

            // If new password is valid
            if (ludoController.isValid(newPassword, true) && ludoController.isValid(repeatedPwd, true)) {

                // If new password and repeated passwords are equal
                if (newPassword.equals(repeatedPwd)) {

                    // Construct message
                    JSONObject message = new JSONObject();
                    message.put(ACTION, "change password");
                    message.put("Password", oldPassword);
                    message.put("NewPassword", newPassword);

                    // Send message
                    try {
                        connection.send(message.toString());
                    } catch (IOException ignored) {}
                } else {
                    changePasswordFeedback.setText(properties.getString("passwordsNotEqual"));
                }
            } else {
                changePasswordFeedback.setText(properties.getString("invalidNewPwd"));
            }
        }
    }

    /**
     * Loads png-image, sends it to server and displays image on screen
     * @param event Button
     */
    @FXML
    void uploadImage(ActionEvent event){

        // Create FileChooser UI, set format to png
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("PNG files", "*.png"));

        try {
            // Open UI
            File uri = fc.showOpenDialog(null);

            // Read image
            FileInputStream fis = new FileInputStream(uri);
            byte[] bytes =  fis.readAllBytes();
            fis.close();

            // Convert image from byte[] to String
            String imgString = Base64.getEncoder().encodeToString(bytes);

            // Construct message
            JSONObject message = new JSONObject();
            message.put(ACTION, "change picture");
            message.put("Image", imgString);

            // Send message
            connection.send(message.toString());


            // Convert from byte[] to Image
            Image image = new Image(new ByteArrayInputStream(bytes));

            // Set image in ludoController
            ludoController.setImage(image);

        } catch(IOException ignored){
            changeImageFeedback.setText(properties.getString("IOErrorPicture"));
        }
    }

    /**
     * Closes edit-tab
     * @param event Button
     */
    @FXML
    void back(ActionEvent event) {
        ludoController.closeEditTab();
    }

    /**
     * Shows whether userName was changed or not on screen
     * This function is called from LudoController(who received message from server)
     * @param userNameChanged boolean, True if userName was changed, false otherwise
     * @param newUserName String, new userName(null if userName was not changed)
     */
    void updateUserName(boolean userNameChanged, String newUserName){

        // Clear feedback-field
        changeNameFeedback.setText("");

        // If username was successfully changed
        if(userNameChanged){

            userName.setText(newUserName);
            changeNameFeedback.setText(properties.getString("nameChangeOK"));

            // Clear input-field
            this.newUsername.setText("");

        } else{
            changeNameFeedback.setText(properties.getString("nameChangeFAIL"));
        }
    }

    /**
     * Shows whether password was changed or not on screen
     * This function is called from LudoController(who received message from server)
     * @param passwordChanged boolean, True if password was changed, false otherwise
     */
    void updatePassword(boolean passwordChanged){

        // Clear feedback-field
        changePasswordFeedback.setText("");

        if(passwordChanged){
            changePasswordFeedback.setText(properties.getString("passwordChangeOK"));

            // Clear input-fields
            oldPwd.setText("");
            newPwd.setText("");
            repeatPwd.setText("");

        } else{
            changePasswordFeedback.setText(properties.getString("passwordChangeFAIL"));
        }
    }

    /**
     * Shows whether image was changed or not on screen
     * @param imageChanged Boolean, true if image was changed, false otherwise
     */
    void updateImage(boolean imageChanged){

        // Clear feedback-field
        changeImageFeedback.setText("");

        if(imageChanged){
            changeImageFeedback.setText(properties.getString("pictureChangeOK"));
        } else{
            changeImageFeedback.setText(properties.getString("pictureChangeFAIL"));
        }
    }

    /**
     * Sets users userName on screen
     * This function is called from LudoController
     * @param name String, userName of user
     */
    @FXML
    void setUserName(String name){
        userName.setText(name);
    }

    /**
     * Displays image
     * @param image Image
     */
    @FXML
    void displayImage(Image image){
        imageView.setImage(image);
    }
}

