package no.ntnu.imt3281.ludo.gui;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;

import javafx.scene.layout.BorderPane;
import no.ntnu.imt3281.ludo.client.ServerConnection;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Controller for the main window of this application
 * Starts with a login/register-tab
 */
public class LudoController {

    @FXML
    private MenuItem random;

    // Tabpanes for chatrooms and home/edit/games
    @FXML
    private TabPane tabbedPane;
    @FXML
    private TabPane chatPane;

    // Loginpage and homescreen
    @FXML
    private Tab loginTab;
    @FXML
    private Tab homeScreenTab;

    // Fields for username and password(login/register)
    @FXML
    private TextField loginUsername;
    @FXML
    private PasswordField loginPassword;
    @FXML
    private TextField registerUsername;
    @FXML
    private PasswordField registerPassword;
    @FXML
    private PasswordField registerRepeat;

    // Feedback if the user does something 'wrong'
    @FXML
    private Label registerFeedback;
    @FXML
    private Label loginFeedback;

    // Connection to server
    private ServerConnection connection;
    // I18N-file
    private ResourceBundle properties;

    // Reference to controllers of tabs opened by this controller
    private HomeScreenController homeScreenController;
    private EditProfileController editProfileController;
    private ChatController globalChatController;

    // References to chat -and gameControllers
    private List<GameBoardController> games = new ArrayList<>();
    private List<ChatController> chats = new ArrayList<>();

    // Used for adding/extracting information to/from server-messages
    private static final String ACTION = "Action";
    private static final String USERNAMEE = "UserName";
    private static final String PASSWORD = "Password"; // NOSONAR because this is not a password
    private static final String TOKEN = "Token";
    private static final String MESSAGE = "Message";
    private static final String GLOBALCHAT = "Global Chat";
    private static final String CHATNAME = "ChatName";
    private static final String GAMEID = "GameID";
    private static final String EVENTTYPE = "EventType";

    // Users name and image
    private String userName;
    private Image image;

     /**
     * Constructor, establishes Connection to server
     **/
    public LudoController(){

        connection = new ServerConnection();
        properties = ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n");
    }

    /**
     * Attemps to automatically log in with token
     * Sets enter-key to login when pressed on password-field
     * Sets enter-key to register when pressed on repeat password-field
     */
    @FXML
    public void initialize() {

        runListener();

        // If token exists
        if (readToken() != null) {
            // Request server for automatic login
            try {
                JSONObject message = new JSONObject();
                message.put(ACTION, "loginAuto");
                message.put(TOKEN, readToken());
                connection.send(message.toString());
            } catch (IOException ignored) {}
        }

        // Try to login when enter-key is pressed in password-field
        loginPassword.setOnKeyPressed(keyEvent -> {
            if(keyEvent.getCode() == KeyCode.ENTER){
                login();
            }
        });

        // Try to register when enter-key is pressed in repeat password-field
        registerRepeat.setOnKeyPressed(keyEvent -> {
            if(keyEvent.getCode() == KeyCode.ENTER){
                register();
            }
        });

        loginTab.setClosable(false);

        registerUsername.setPromptText(properties.getString("registerUser.prompt"));
        registerPassword.setPromptText(properties.getString("registerPwd.prompt"));
        registerRepeat.setPromptText(properties.getString("registerPwd.prompt"));
    }

    /**
     * Continuously istens for messages from server
     */
    private void runListener(){

        Thread thread = new Thread(() -> {
            while(true) {
                try {
                    String msg = connection.read();
                    // If there is a message:
                    if (msg != null) {
                        JSONObject message = stringToJSON(msg);
                        Platform.runLater(() -> handleMessage(message));
                    }
                } catch (IOException ignored) {
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * Handles messages from server(calls correct functions)
     * @param message JSONObject, message to handle(keyword 'Action')
     */
    private void handleMessage(JSONObject message){

        String action = message.get(ACTION).toString();

        switch(action) {
            case "newUserOK":
            case "loginManualOK":
                userName = message.get(USERNAMEE).toString();
                writeToken(message.get(TOKEN).toString());
                openHomeScreenTab(message);
                openGlobalChat(message);
                break;

            case "loginAutoOK":
                userName = message.get(USERNAMEE).toString();
                openHomeScreenTab(message);
                openGlobalChat(message);
                break;

            case "newUserFAIL":
                registerFeedback.setText(properties.getString(message.get(MESSAGE).toString()));
                break;

            case "loginManualFAIL":
                loginFeedback.setText(properties.getString(message.get(MESSAGE).toString()));
                break;

            case "nameChangeOK":
            case "nameChangeFAIL":
                updateUserName(message);
                break;

            case "passwordChangeOK":
            case "passwordChangeFAIL":
                updatePassword(message);
                break;

            case "pictureChangeOK":
                editProfileController.displayImage(image);
                editProfileController.updateImage(true);
                break;

            case "pictureChangeFAIL":
                image = null;
                editProfileController.updateImage(false);
                break;

            case "playerJoined":
                checkGames(message);
                break;

            case "playerLeftGame":
                playerLeftGame(message);
                break;

            case "event":
                handleEvent(message);
                break;

            case "gameChat":
                sendGameChat(message);
                break;

            case "createChatOK":
            case "joinChatOK":
                openChat(message);
                break;

            case "createChatFAIL":
                homeScreenController.createChatRoomFail();
                break;

            case "chat":
                incomingChat(message);
                break;

            case "userJoinedChat":
            case "userLeftChat":
                userJoinedOrLeftChat(message);
                break;

            case "friendRequestOK":
            case "friendRequestFAIL":
                homeScreenController.friendRequestFeedback(message);
                break;

            case "friendAdded":
            case "friendAcceptOK":
                homeScreenController.addFriendToList(message);
                break;

            case "refresh":
                homeScreenController.refresh(message);
                break;

            default:
                break;
        }

        if (action.matches("newUserOK|loginManualOK|loginAutoOK")) {
            setUserNameAndImage(message, action);
        }
    }

    /**
     * Sets userName and image after user logged in or registered
     * User may not have a image(="")
     * @param message JSONObject containing UserName and Image
     * @param action String, 'newUserOK'/'loginManualOK'/'loginAutoOK'
     */
    private void setUserNameAndImage(JSONObject message, String action) {

        // Extract userName and image as String
        userName = message.get(USERNAMEE).toString();

        // If user logged in
        if(!"newUserOK".equals(action)) {

            String byteString = message.get("Image").toString();

            // If user has an image
            if ((!"".equals(byteString))) {

                // Convert String to byte[]
                byte[] bytes = Base64.getDecoder().decode(byteString);

                // Set image
                image = new Image(new ByteArrayInputStream(bytes));
            }
        } else {
            image = null;
        }
    }

    /**
     * Sets image of user(called from EditProfileController)
     * @param img Image
     */
    void setImage(Image img) {
        image = img;
    }

    /**
     * Tells the server that this client wants to join a random game
     */
    @FXML
    void joinRandomGame() {
        // If user is logged in
        if(tabbedPane.getTabs().size() > 1) {
            try {
                JSONObject message = new JSONObject();
                message.put(ACTION, "joinRandomGame");
                connection.send(message.toString());
            } catch (IOException ignored) {}
        }
    }

    /**
     * Open Game-tab
     */
    private void checkGames(JSONObject message) {
        int messageID = Integer.parseInt(message.get(GAMEID).toString());
        boolean newGame = true;
        GameBoardController c = null;
        
        if (!games.isEmpty()) {
            for (GameBoardController gc : games) {
                if (messageID == gc.getId()) {
                    c = gc;
                    newGame = false;
                    break;
                }
            }
        }
        
        
        if (newGame) {
            openGameTab(message);
        } else {
            c.updateUsers(message);
        }
    }

    /**
     * Updates GameBoard that a user left before the game has started
     * @param message JSONObject containing 'GameID' and 'UserName'
     */
    private void playerLeftGame(JSONObject message){

        int id = Integer.parseInt(message.get(GAMEID).toString());

        for(GameBoardController gc : games){
            if(gc.getId() == id){
                gc.playerLeft(message);
                break;
            }
        }
    }

    /**
     * Sends the event to the correct handler
     * @param message JSONObject containing an event
     */
    private void handleEvent(JSONObject message) {
        if ("diceEvent".equals(message.get(EVENTTYPE).toString())) {
            diceEvent(message);
        }
        if ("playerEvent".equals(message.get(EVENTTYPE).toString())) {
            playerEvent(message);
        }
        if ("pieceEvent".equals(message.get(EVENTTYPE).toString())) {
            pieceEvent(message);
        }
    }

    /**
     * Finds the correct gameBoardController and tells it to throw the dice with number from server
     * @param message JSONObject containing a diceEvent
     */
    private void diceEvent(JSONObject message) {
        for (GameBoardController gc : games) {
            if (Integer.parseInt(message.get(GAMEID).toString()) == gc.getId()) {
                gc.throwDice(Integer.parseInt(message.get("DiceNumber").toString()));
            }
        }
    }

    /**
     * Finds the correct gameBoardController and tells it to change a players status
     * @param message JSONObject containing a playerEvent
     */
    private void playerEvent(JSONObject message) {
        for (GameBoardController gc : games) {
            if (Integer.parseInt(message.get(GAMEID).toString()) == gc.getId()) {
                gc.playerStateChange(Integer.parseInt(message.get("ActivePlayer").toString()), Integer.parseInt(message.get("Status").toString()));
            }
        }
    }

    /**
     * Finds the correct gameBoardController and tells it to move a piece
     * @param message JSONObject containing a pieceEvent
     */
    private void pieceEvent(JSONObject message) {
        for (GameBoardController gc : games) {
            if (Integer.parseInt(message.get(GAMEID).toString()) == gc.getId()) {
                int activePlayer = Integer.parseInt(message.get("ActivePlayer").toString());
                int fromPos = Integer.parseInt(message.get("FromPos").toString());
                int toPos = Integer.parseInt(message.get("ToPos").toString());
                int pieceNumber = Integer.parseInt(message.get("PieceNumber").toString());

                gc.movePiece(activePlayer, fromPos, toPos, pieceNumber);
            }
        }
    }

    /**
     * Opens a game-tab with the gameID from the message
     * @param message JSONObject, String message from server
     */
    private void openGameTab(JSONObject message) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("GameBoard.fxml"));
        loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
        
        try {
            AnchorPane gameBoard = loader.load();

            GameBoardController controller = loader.getController();
            controller.initialize(connection, this, userName);
            String gameID = message.get(GAMEID).toString();

            // Create tab and set content
            Tab tab = new Tab("Game " + gameID);
            tab.setId(gameID);
            tab.setContent(gameBoard);

            tab.setOnClosed(c -> leaveGame(Integer.parseInt(gameID), false));

            // Add and select the game-tab
            games.add(controller);
            tabbedPane.getTabs().add(tab);
            tabbedPane.getSelectionModel().select(tab);

            // Tell the controller its ID and to start the waiting process
            controller.setId(gameID);
            controller.startWait(message);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Opens and selects HomeScreen-tab
     * @param message JSONObject containing 'UserName', 'noOfGames' and 'noOfVictories', 'Games', 'Victories', 'Friends' and 'ChatNames'
     */
    private void openHomeScreenTab(JSONObject message) {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("HomeScreen.fxml"), properties);

        try {
            // Load, get controller and initialize HomeScreen
            BorderPane homeScreen = loader.load();
            homeScreenController = loader.getController();
            homeScreenController.initialize(connection, this, properties, message);

            // Create tab and set content
            homeScreenTab = new Tab("Home");
            homeScreenTab.setId("Home");
            homeScreenTab.setContent(homeScreen);
            homeScreenTab.setClosable(false);

            // Add and select HomeScreen-tab
            tabbedPane.getTabs().add(homeScreenTab);
            tabbedPane.getSelectionModel().select(homeScreenTab);

            // Disable login/register-tab(clears all text as well)
            disableAndClearLoginTab();

        } catch(IOException ignored) {}
    }

    /**
     * Opens global chat
     * @param message JSONObject containing GlobalChatParticipants
     */
    private void openGlobalChat(JSONObject message) {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("Chat.fxml"), properties);

        try {
            AnchorPane globalChat = loader.load();

            List participants = (List) message.get("GlobalChatParticipants");
            participants.add(userName);
            ObservableList obsList = FXCollections.observableArrayList();
            obsList.addAll(participants);

            globalChatController = loader.getController();
            globalChatController.initialize(connection, properties, obsList);
            globalChatController.setChatName(GLOBALCHAT);

            chats.add(globalChatController);

            // Create tab and set content
            Tab tab = new Tab(GLOBALCHAT);
            tab.setClosable(false);
            tab.setId("globalChat");
            tab.setContent(globalChat);

            // Add tab
            chatPane.getTabs().add(tab);

        } catch(IOException ignored) {}
    }

    /**
     * Opens chat as tab
     */
    private void openChat(JSONObject message) {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("Chat.fxml"), properties);

        try {
            AnchorPane chat = loader.load();

            String action = message.get(ACTION).toString();
            String name = message.get(CHATNAME).toString();

            ChatController chatController = loader.getController();

            // If user joined chat
            if("joinChatOK".equals(action)) {
                List participants = (List) message.get("Participants");
                ObservableList obsParticipants = FXCollections.observableArrayList(participants);
                chatController.initialize(connection, properties, obsParticipants);
                // If user created chat
            } else {
                ObservableList obsParticipants = FXCollections.observableArrayList();
                obsParticipants.add(userName);
                chatController.initialize(connection, properties, obsParticipants);
                homeScreenController.addChatRoomToList(message);
            }

            chatController.setChatName(name);

            // Add chatController to list of chatControllers
            chats.add(chatController);

            // Create tab and set content
            Tab tab = new Tab(name);
            tab.setId(name);
            tab.setContent(chat);

            // Leave chat when user closes tab
            tab.setOnClosed(c -> leaveChat(name));

            // Add and select tab
            chatPane.getTabs().add(tab);
            chatPane.getSelectionModel().select(tab);

        } catch(IOException ignored) {}
    }

    /**
     * Opens and selects EditProfile-tab
     */
    void openEditTab() {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("EditProfile.fxml"), properties);

        try {
            BorderPane editProfile = loader.load();

            // Get controller, initialize, set and display image
            editProfileController = loader.getController();
            editProfileController.initialize(connection, this, properties);
            editProfileController.setUserName(userName);
            editProfileController.displayImage(image);

            // Create tab and set content
            Tab tab = new Tab("Edit");
            tab.setId("Edit");
            tab.setContent(editProfile);

            // Add and select EditProfile-tab
            tabbedPane.getTabs().add(tab);
            tabbedPane.getSelectionModel().select(tab);

            // Disable homeScreen-tab
            homeScreenTab.setDisable(true);

        } catch(IOException ignored) {}
    }

    /**
     * Closes EditProfile-tab
     */
    void closeEditTab() {
        for(Tab tab : tabbedPane.getTabs()){
            if("Edit".equals(tab.getId())){
                tabbedPane.getTabs().remove(tab);
                break;
            }
        }
        homeScreenTab.setDisable(false);
        tabbedPane.getSelectionModel().select(homeScreenTab);
    }

    /**
     * Messages server that user left chat
     */
    @FXML
    private void leaveChat(String chatName) {

        try {
            JSONObject message = new JSONObject();
            message.put(ACTION, "leaveChat");
            message.put(CHATNAME, chatName);
            connection.send(message.toString());
        } catch (IOException ignored) {

        }
    }

    /**
     * Closes/Leaves a game
     * @param gameID int, The game to be closed
     * @param autoClose bool, true if there was not enough players
     */
    void leaveGame(int gameID, boolean autoClose) {
        try {
            JSONObject message = new JSONObject();
            message.put(ACTION, "leaveGame");
            message.put(GAMEID, gameID);
            connection.send(message.toString());

            // Refresh so HomeScreen is updated
            message = new JSONObject();
            message.put(ACTION, "refresh");
            connection.send(message.toString());
        } catch (IOException ignored) {}

        for (GameBoardController gameBoardController : games) {
            if (gameBoardController.getId() == gameID) {
                games.remove(gameBoardController);
                gameBoardController.setClosed();
                break;
            }
        }

        if (autoClose) {
            Tab removeThis = null;
            for (Tab tab : tabbedPane.getTabs()) {
                if (tab.getId().equals(String.valueOf(gameID))) {
                    removeThis = tab;
                }
            }
            tabbedPane.getTabs().remove(removeThis);
        }
    }

    /**
     * When another user joins or leaves chat, update list of chatParticipants in ChatController
     * @param message JSONObject containing Action 'userJoinedChat'/'userLeftChat', ChatName and userName
     */
    private void userJoinedOrLeftChat(JSONObject message) {

        // Extract action, chatName and userName from message
        String action = message.get(ACTION).toString();
        String chatName = message.get(CHATNAME).toString();
        String userNamee = message.get(USERNAMEE).toString();

        // Decide whether user joined or left chat
        boolean joined = false;
        if("userJoinedChat".equals(action)) {
            joined = true;
        }

        // Find correct chat
        for(ChatController chat : chats) {
            if(chat.getChatName().equals(chatName)) {

                chat.userJoinedOrLeft(userNamee, joined);
                break;
            }
        }
    }

    /**
     * Tries to login manually
     */
    @FXML
    void login() {

        // Clear feedback-fields
        loginFeedback.setText("");
        registerFeedback.setText("");

        // Extract username and password
        String name = loginUsername.getText();
        String password = loginPassword.getText();

        // If username and/or password is empty
        if(!name.isEmpty() && !password.isEmpty()) {
            // If the username and password fits the criteria
            if (isValid(name, false) && isValid(password, true)) {

                // Construct login-message
                JSONObject message = new JSONObject();
                message.put(ACTION, "loginManually");
                message.put(USERNAMEE, name);
                message.put(PASSWORD, password);

                // Send message to server
                try {
                    connection.send(message.toString());
                } catch (IOException ignored) {
                }
            } else {
                loginFeedback.setText(properties.getString("login.feedback"));
            }
        } else {
            loginFeedback.setText(properties.getString("login.emptyFields"));
        }
    }

    /**
     * Tries to register a new user
     */
    @FXML
    void register() {

        // Clear feedback-fields
        registerFeedback.setText("");
        loginFeedback.setText("");

        String name = registerUsername.getText();
        String password = registerPassword.getText();

        // If username and/or password is empty
        if(!name.isEmpty() && !password.isEmpty()) {
            // If the username and password fits the criteria, and the passwords match
            if (isValid(name, false) && isValid(password, true) && password.equals(registerRepeat.getText())) {

                // Construct message
                JSONObject message = new JSONObject();
                message.put(ACTION, "new user");
                message.put(USERNAMEE, name);
                message.put(PASSWORD, password);

                // Send message to server
                try {
                    connection.send(message.toString());
                } catch (IOException e) {
                }
            } else {
                registerFeedback.setText(properties.getString("register.feedback"));
            }
        } else {
            registerFeedback.setText(properties.getString("register.emptyFields"));
        }
    }

    /**
     * Gamechat, Goes through all the open games on this client
     * and sends the chatmessage to the correct game
     * @param message JSONObject, String message from server
     */
    private void sendGameChat(JSONObject message) {
        for (GameBoardController game : games) {
            if (game.getId() == Integer.parseInt(message.get(GAMEID).toString())) {
                game.setChatText(message.get("Sender").toString() + ": " + message.get(MESSAGE));
                break;
            }
        }
    }

    /**
     * Receives the chat message and sends it to the correct chatController
     * @param message JSONObject, String message from server
     */
    private void incomingChat(JSONObject message) {
        String chatName = message.get(CHATNAME).toString();

        if (GLOBALCHAT.equals(chatName)) {
            globalChatController.receiveChat(message);
        } else {
            for (ChatController chat : chats) {
                if (chat.getChatName().equals(chatName)) {
                    chat.receiveChat(message);
                }
            }
        }
    }

    /**
     * Disables Login-tab and clears all text
     */
    private void disableAndClearLoginTab() {

        // Disable Login-tab
        loginTab.setDisable(true);

        // Clear login-related text-fields
        loginUsername.setText("");
        loginPassword.setText("");
        loginFeedback.setText("");

        // Clear register-related text-fields
        registerUsername.setText("");
        registerPassword.setText("");
        registerRepeat.setText("");
        registerFeedback.setText("");
    }

    /**
     * Logs out user(closes Home-tab, enables Login-tab, sends message to server and creates new ServerConnection)
     */
    @FXML
    public void logout() {

        // If user is logged in(HomeScreen-tab is open)
        if(tabbedPane.getTabs().size() > 1) {

            // Enable Login/register-tab
            loginTab.setDisable(false);

            // HomeScreen, editProfile and globalChat are all closed, set null
            homeScreenController = null;
            editProfileController = null;
            globalChatController = null;

            // Remove image
            image = null;

            // Get a list of all tabs except login/register-tab
            List<Tab> tabs = new ArrayList<>();
            for(int i = 1; i < tabbedPane.getTabs().size(); i++) {
                tabs.add(tabbedPane.getTabs().get(i));
            }
            // Remove tabs
            for(Tab tab : tabs) {
                tabbedPane.getTabs().remove(tab);
            }

            // Remove all chat-tabs
            chatPane.getTabs().clear();

            // Construct logout-message
            JSONObject message = new JSONObject();
            message.put(ACTION, "logout");

            // Send logout-message and close ServerConnection
            try {
                connection.send(message.toString());
                connection.close();
            } catch (IOException ignored) {}

            // Create new ServerConnection
            connection = new ServerConnection();
        }
    }

    /**
     * Sends exit-message to server and closes ServerConnection
     * Is called from Client when user closes window
     */
    public void exitProgram() {

        JSONObject message = new JSONObject();
        message.put(ACTION, "exit");

        try {
            connection.send(message.toString());
            connection.close();
        } catch(IOException ignored) {}
    }

    /**
     * Handles servers answer for changing userName
     * Passes true and newUserName, or false to EditProfile-tab, so it can show update on screen
     * @param message JSONObject containing Action 'nameChangeOK', or 'nameChangeFAIL' and Message if userName was not changed
     */
    private void updateUserName(JSONObject message) {

        boolean userNameChanged = false;
        String newUserName = null;

        String action = message.get(ACTION).toString();

        if("nameChangeOK".equals(action)) {
            newUserName = message.get(USERNAMEE).toString();
            userName = newUserName;
            userNameChanged = true;
        }
        editProfileController.updateUserName(userNameChanged, newUserName);
        homeScreenController.setUserName(newUserName);
    }

    /**
     * Handles servers answer for changing password
     * Passes true or false to EditProfile-tab, so it can show update on screen
     * @param message JSONObject containing Action 'passwordChangeOK' or 'passwordChangeFAIL'
     */
    private void updatePassword(JSONObject message) {

        boolean passwordChanged = false;

        String action = message.get(ACTION).toString();

        if("passwordChangeOK".equals(action)) {
            passwordChanged = true;
        }

        editProfileController.updatePassword(passwordChanged);
    }

    /**
     * Manages the different checks for the different fields
     * @param input String, input from user to check
     * @param isPassword Boolean, whether it is password
     * @return True if every constraint and demands is met, false if not
     */
    boolean isValid(String input, boolean isPassword) {

        if (!isPassword) {
            // For username
            // Constraints | 5-20 length | a-z | A-Z | 0-9
            int validChars = 0;
            if (!inBetween(input.length(), 5,20)) {
                return false;
            }
            validChars += nrOfOnlyValidChars(input, 65, 90);
            validChars += nrOfOnlyValidChars(input, 97, 122);
            validChars += nrOfOnlyValidChars(input, 48, 57);
            return validChars == input.length();
        } else {
            // For password
            // Demands | 8-30 length, at least one: lowercase, uppercase, number
            int validChars = 0;
            if(!inBetween(input.length(), 8, 30)) {
                return false;
            }
            if(!meetsDemand(input, 65, 90)) {
                return false;
            }
            if(!meetsDemand(input, 97, 122)) {
                return false;
            }
            if(!meetsDemand(input, 48, 57)) {
                return false;
            }
            // Constraints | a-z | A-Z | 0-9
            validChars += nrOfOnlyValidChars(input, 65,90);
            validChars += nrOfOnlyValidChars(input, 97,122);
            validChars += nrOfOnlyValidChars(input, 48,57);
            return validChars == input.length();
        }
    }

    /**
     * Checks if a length is between two values
     * @param length Int, length
     * @param min Int, minimum length
     * @param max Int, maximum length
     * @return True if length is between min and max, false if not
     */
    private boolean inBetween(int length, int min, int max) {
        return length >= min && length <= max;
    }

    /**
     * Counts the number of valid chars
     * @param input String, input from user
     * @param min Int, minimum value of char (in ascii)
     * @param max Int, maximum value of char (in ascii)
     * @return Int, Number of valid chars
     */
    private int nrOfOnlyValidChars(String input, int min, int max) {
        int validChars = 0;

        // Since this is run for three different checks, but on the same string,
        //// we need to sum up the valid chars in each check and see if it is the
        //// same length as the original string
        for (int i = 0; i < input.length(); i++) {
            int c = input.charAt(i);
            if (inBetween(c, min, max)) {
                validChars++;
            }
        }
        return validChars;
    }

    /**
     * Check if the input meets demands set
     * @param input String, input from the user
     * @param min Int, minimum value of char (in ascii)
     * @param max Int, maximum value of char (in ascii)
     * @return Boolean, true if a minimum of one char exists of the given demands (min and max)
     */
    private boolean meetsDemand(String input, int min, int max) {
        for (int i = 0; i < input.length(); i++) {
            int c = input.charAt(i);
            if (inBetween(c, min, max)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Writes a given token to file (.txt)
     * @param content String, the token
     */
    private static void writeToken(String content) {
        OutputStream stream = null;
        try {
            stream = new FileOutputStream(new File("src/token.txt"));
            stream.write(content.getBytes(), 0, content.length());
        } catch (IOException ignored) {

        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (IOException e) {}
        }
    }

        /**
         * Attempts to read the token from file
         * @return String, token if it exists, null if not
         */
        private static String readToken() {
            String token;
            try {
                token = new String(Files.readAllBytes(Paths.get("src/token.txt")));
            } catch (IOException ignored) {
                return null;
            }
            return token;
        }

    /**
     * Converts string to JSONObject
     * @param str String to be converted
     * @return JSONObject with str contents
     */
    private JSONObject stringToJSON(String str) {

        JSONObject message = null;
        try {
            JSONParser parser = new JSONParser();
            message = (JSONObject) parser.parse(str);
        } catch (ParseException ignored) {
            // Exception does not happen, str is not null(checked in runListener()) and server-messages are thoroughly tested
        }
        return message;
    }
}