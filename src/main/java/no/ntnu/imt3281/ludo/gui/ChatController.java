package no.ntnu.imt3281.ludo.gui;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import no.ntnu.imt3281.ludo.client.ServerConnection;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * Controller for globalchat and
 * other chat rooms
 */
public class ChatController {

    // The name of the chatroom
    @FXML
    private Label chatNameLabel;

    // The area that the chat is displayed in
    @FXML
    private TextArea chatArea;

    // List to show the players in the chatroom
    @FXML
    private ListView<String> playersInChatListView;

    // Field for writing to chat
    @FXML
    private TextField inputChat;

    private ServerConnection connection;
    private ResourceBundle properties;

    private String chatName;
    private ObservableList obsParticipants;

    // Timestamp the messages in chat
    private SimpleDateFormat time = new SimpleDateFormat("HH:mm");

    /**
     * Initializes chat: Sets ServerConnection and internationalization file
     * @param con ServerConnection, used to send messages to server
     * @param i18N ResourceBundle, internationalization file
     * @param players ObservableList of already participating users
     */
    void initialize(ServerConnection con, ResourceBundle i18N, ObservableList players){

        connection = con;
        properties = i18N;
        obsParticipants = players;

        // sendChat when enter-key pressed in inputChat-field
        inputChat.setOnKeyPressed(keyEvent -> {
            if(keyEvent.getCode() == KeyCode.ENTER){
                sendChat();
            }
        });

        playersInChatListView.setItems(obsParticipants);
    }

    /**
     * Sets name of chat
     * @param name String, name to be set
     */
    void setChatName(String name){

        chatName = name;
        chatNameLabel.setText(name);
    }

    /**
     * @return name of chat
     */
    String getChatName() { return chatName; }

    /**
     * Sends chatmessage from user
     */
    @FXML
    void sendChat() {
        // Extract message
        String msg = inputChat.getText();

        // If there is a message
        if(msg.length() > 0){

            // Construct message
            JSONObject message = new JSONObject();
            message.put("Action", "chat");
            message.put("ChatName", chatName);
            message.put("Message", msg);

            // Send message
            try{
                connection.send(message.toString());
            } catch(IOException ignored){
                chatArea.appendText(properties.getString("couldNotSend"));
            }
            // Empty inputField
            inputChat.setText("");
        }
    }

    /**
     * Put received message to to global-chat on screen
     * @param message JSONObject containing Sender and Message
     */
    @FXML
    void receiveChat(JSONObject message){

        // Extract sender and message
        String sender = message.get("Sender").toString();
        String msg = message.get("Message").toString();

        // Add to screen
        chatArea.appendText("[" + time.format(new Date()) + "]: " + sender + ": " + msg + "\n");
    }

    /**
     * Adds/removes userName of user who joined/left chat to/from list
     * @param userName String, name of user
     * @param joined True if user joined, false if user left
     */
    @FXML
    void userJoinedOrLeft(String userName, boolean joined){

        // If a player joined
        if(joined){
            obsParticipants.add(userName);
        // A player left
        } else{
            obsParticipants.remove(userName);
        }
    }
}
