package no.ntnu.imt3281.ludo.server;

import no.ntnu.imt3281.ludo.logic.Ludo;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Game object
 */
class Game {

    private int gameId;
    private int timer;
    private List<String> players;
    private List<ClientConnection> clients;
    private boolean hasStarted;
    // Ludo-object
    private Ludo ludo;
    private static final Logger LOGGER = MyLogger.getLogger();

    /**
     * Constructor, sets is and initializes timer, hasStarted and players
     * @param id int, unique id of game
     */
    Game(int id) {
        gameId = id;
        timer = 0;
        hasStarted = false;
        players = new ArrayList<>();
        clients = new ArrayList<>();
    }

    /**
     * Adds a player(userName) to game
     * @param userName String, userName to be added
     */
    void addPlayer(String userName, ClientConnection connection) {
        players.add(userName);
        clients.add(connection);
    }

    /**
     * Removes a player
     * @param clientConnection Client connection
     */
    void removePlayer(ClientConnection clientConnection) {
        clients.remove(clientConnection);
        if (hasStarted) {
            ludo.removePlayer(clientConnection.getUserName());
            ludo.removeListeners(clientConnection);
        } else{
            JSONObject message = new JSONObject();
            message.put("Action", "playerLeftGame");
            message.put("GameID", gameId);
            message.put("UserName", clientConnection.getUserName());
            for(ClientConnection c : clients){
                try{
                    c.send(message.toString());
                } catch(IOException e){
                    LOGGER.log(Level.INFO, e.getMessage(), e);
                }
            }
        }
        players.remove(clientConnection.getUserName());
    }

    /**
     * @return number of players in game
     */
    int getNrOfPlayers() { return(players.size()); }

    /**
     * Starts a timer counting to 1 minute
     */
    void startTimer() {
        Runnable runTimer = () -> {
            while (timer < 60) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ignored) {}
                timer++;
            }
        };
        Thread thread = new Thread(runTimer);
        thread.start();
    }

    /**
     * @return id of game
     */
    int getGameId() { return gameId; }

    /**
     * @return how long timer has counted
     */
    int getTimer() { return timer; }

    /**
     * @return list of players in game
     */
    List getPlayersJoined() { return players; }

    /**
     * @return True if game has started, false otherwise
     */
    boolean hasStarted() { return hasStarted; }

    /**
     * Starts the game with the current list of players
     * @param players List of players
     */
    void startGame(List<String> players) {
        hasStarted = true;
        ludo = new Ludo(players.get(0), players.get(1), players.get(2), players.get(3));
        ludo.setId(gameId);
        // Adds listeners for every client
        for (ClientConnection client : clients) {
            ludo.addDiceListener(client);
            ludo.addPieceListener(client);
            ludo.addPlayerListener(client);
        }
    }

    /**
     * Throws the dice which in turn generates dice events
     */
    void throwDice() {
        ludo.throwDice();
    }

    /**
     * Moves a piece on the servers ludo
     * @param message JSONObject containing info to move a piece
     */
    void movePiece(JSONObject message) {
        int localPos = Integer.parseInt(message.get("LocalPos").toString());
        int localPosTo = Integer.parseInt(message.get("LocalPosTo").toString());

        ludo.movePiece(Integer.parseInt(message.get("ActivePlayer").toString()) , localPos, localPosTo);
    }
}
