package no.ntnu.imt3281.ludo.server;

import java.util.logging.Logger;

/**
 * A Logger class to log errors in server
 * Uses getLogger() to always get the same logger object
 */
class MyLogger {

    private static final Logger LOGGER = Logger.getLogger(MyLogger.class.getPackage().toString());

    /**
     * Gets a Logger object
     * @return a Logger object
     */
    static Logger getLogger() {
        return LOGGER;
    }
}