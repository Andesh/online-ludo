package no.ntnu.imt3281.ludo.server;

import io.jsonwebtoken.*;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.crypto.spec.SecretKeySpec;
import javax.sql.rowset.serial.SerialBlob;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.*;
import java.text.Collator;
import java.util.*;
import java.util.Date;
import java.util.Timer;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.*;

/**
 * This is the main class for the server.
 * Connects to embedded Derby database
 * Supports communication with clients
 * Supports Chatroom-functionality and a Global Chat
 * Supports playing Ludo-games
 * Supports friend-functionality
 */
public class Server {

    // List of clients
    private final List<ClientConnection> clientList;

    // List of ChatRooms and the global ChatRoom
    private List<ChatRoom> chatRoomList;
    private ChatRoom globalChat;

    // List of Games and id of next Game
    private List<Game> gameList;
    private int gameIds;

    // Connection to embedded Derby Database
    private Connection connection;

    // Logger and chatLOGGER(to file)
    private static final Logger LOGGER = MyLogger.getLogger();
    private static final Logger CHATLOGGER = Logger.getLogger(Server.class.getName());

    // JSON Web Token signing key
    private static final String SECRETKEY = "3sdf8sdf89sdfS8sa98fFef9g8Gjtyi9Ho8hjhgtg98090kUHlksa";
    // Password for DB
    private static final String DBPWD = "rai9y118ddYoK";
    // Algorithm used for hashing
    private static final String HASHALGORITHM = "SHA-256";

    // Used for client/server communication:
    private static final String ACTION = "Action";
    private static final String USERNAME = "UserName";
    private static final String PASSWORD = "Password"; // NOSONAR because this is not a password
    private static final String TOKEN = "Token";
    private static final String NOOFGAMES = "noOfGames";
    private static final String NOOFVICTORIES = "noOfVictories";
    private static final String MESSAGE = "Message";
    private static final String IMAGE = "Image";
    private static final String NAMECHANGEOK = "nameChangeOK";
    private static final String NAMECHANGEFAIL = "nameChangeFAIL";
    private static final String PLAYERJOINED = "playerJoined";
    private static final String GAMES = "Games";
    private static final String VICTORIES = "Victories";
    private static final String CREATECHATOK = "createChatOK";
    private static final String CREATECHATFAIL = "createChatFAIL";
    private static final String GLOBALCHATT = "Global Chat";
    private static final String CHATNAME = "ChatName";
    private static final String USERLEFTCHAT = "userLeftChat";
    private static final String REFRESH = "refresh";
    private static final String FRIENDREQUESTFAIL = "friendRequestFAIL";
    private static final String ERROR = "error";
    private static final String GAMEID = "GameID";

    /**
     * Connects to/creates an embedded Derby database
     * Runs ServerSocket and starts listening for client-messages
     * Initializes lists and loggers
     * @param dbConnection Optional, Connection to a Derby database or null.
     */
    Server(Connection dbConnection) {

        // for setting logging level
        LOGGER.setLevel(Level.INFO);
        CHATLOGGER.setLevel(Level.INFO);

        // Make chatLOGGER log to file
        CHATLOGGER.setUseParentHandlers(false);
        // Create formatter and FileHandler, add FileHandler to chatLogger
        try {
            SimpleFormatter formatter = new SimpleFormatter();
            FileHandler fh = new FileHandler("src/Log.txt");
            fh.setFormatter(formatter);
            CHATLOGGER.addHandler(fh);
        } catch(IOException e){
            LOGGER.log(Level.SEVERE, "Filehandler for logger: " + e.getMessage(), e);
            System.exit(1);
        }

        if(dbConnection == null) {
            // Attempt to connect to DB
            try {
                connection = DriverManager.getConnection("jdbc:derby:./LudoDB;user=root;password="+ DBPWD);
            } catch (SQLException e) {
                // Connection failed
                // If DB does not exist
                if("Database './LudoDB' not found.".equals(e.getMessage())) {
                    // Attempt to create DB
                    try {
                        connection = DriverManager.getConnection("jdbc:derby:./LudoDB;create=true;user=root;password="+ DBPWD);
                        DatabaseSetup.setDBPassword(connection, DBPWD);
                        DatabaseSetup.createTables(connection);
                    } catch (SQLException e1){
                        // DB could not be created, exit
                        LOGGER.log(Level.SEVERE, "Could not create database " + e1.getMessage(), e1);
                        System.exit(1);
                    }
                } else {
                    // DB exists, but could not connect
                    LOGGER.log(Level.SEVERE, "Could not connect to database " + e.getMessage(), e);
                    System.exit(1);
                }
            }
        } else {
            connection = dbConnection;
        }

        clientList = new CopyOnWriteArrayList<>();
        gameList = new ArrayList<>();
        gameIds = 1;

        // Read chatRooms into memory
        chatRoomList = getChatRoomsFromDb();
        if(chatRoomList == null) {
            chatRoomList = new ArrayList<>();
        }
        globalChat = new ChatRoom(GLOBALCHATT);

        // Run server and listen for messages from clients
        runServerThread();
        runListener();
    }

    /**
     * Starts ServerSocket
     * Adds clients(ClientObj) to clientList after connection
     */
    private void runServerThread() {

        Thread serverThread = new Thread(() -> {
            try(ServerSocket server = new ServerSocket(9010)) {

                while(true) {
                    Socket s = server.accept();
                    String remote = s.getInetAddress().getHostName();
                    int remotePort = s.getPort();

                    LOGGER.log(Level.INFO,"Connected to: " + remote + " on port: " + remotePort);

                    // Add client to list
                    addClient(s, remote);
                }
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
                System.exit(-1);
            }
        });
        // Start thread
        serverThread.start();
    }

    /**
     * Periodically listens for messages sent by clients
     */
    private void runListener() {

        TimerTask checkActivity = new TimerTask() {

            @Override
            public void run() {
            synchronized (clientList) {

                // Go through all clients
                for (ClientConnection client : clientList) {

                    try {
                        String msg = client.read();
                        // If there is a message:
                        if (msg != null) {
                            // Handle message
                            handleMessage(msg, client);
                        }
                    } catch (IOException e) {
                        LOGGER.log(Level.WARNING, e.getMessage(), e);
                    }
                }
            }
            }
        };
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(checkActivity, 50L, 50L);
    }

    /**
     * Adds a ClientObj to clientList
     * @param s Socket
     * @param remote hostName
     */
    private void addClient(Socket s, String remote) {

        try {
            ClientConnection client = new ClientConnection(s, remote);
            synchronized(clientList) {
                clientList.add(client);
            }
        } catch(IOException e){
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }
    }

    /**
     * Handles messages from clients(calls correct functions)
     * @param msg String, message to handle(JSON-format with keyword Action)
     * @param client ClientObj, client who sent the message
     */
    private void handleMessage(String msg, ClientConnection client) {

        // Convert message to JSONObject, get 'Action'-command
        JSONObject message = stringToJSON(msg);
        String action = message.get(ACTION).toString();
        String returnAction = null;

        switch (action) {
            case "new user":
                String token = newUser(message);
                client.setUserName(message.get(USERNAME).toString());
                returnAction = (token != null) ? "newUserOK":"newUserFAIL";
                messageClient(returnAction, client, token,  client.getUserName(), null);
                break;

            case "loginManually":
                token = loginManually(message);
                client.setUserName(message.get(USERNAME).toString());
                returnAction = (token != null) ? "loginManualOK":"loginManualFAIL";
                messageClient(returnAction, client, token, client.getUserName(), null);
                break;

            case "loginAuto":
                String userName = null;
                String[] array = loginAutomatically(message);
                returnAction = array[0];

                if("loginAutoOK".equals(returnAction)) {
                    userName = array[1];
                    client.setUserName(userName);
                }
                messageClient(returnAction, client, null, userName, null);
                break;

            case "change username":
                    userName = null;
                    returnAction = (changeUserName(message, client.getUserName())) ? NAMECHANGEOK : NAMECHANGEFAIL;
                    if(returnAction.equals(NAMECHANGEOK)) {
                        userName = message.get("NewUserName").toString();
                    }
                    messageClient(returnAction, client, userName, null, null);
                break;

            case "change password":
                returnAction = (changePassword(message, client.getUserName()))? "passwordChangeOK":"passwordChangeFAIL";
                messageClient(returnAction, client, null, null, null);
                break;

            case "change picture":
                returnAction = (updateUserImage(message, client.getUserName())) ? "pictureChangeOK":"pictureChangeFAIL";
                messageClient(returnAction, client, null, null, null);
                break;

            case "createChat":
                String chatName = null;
                List<String> participants = null;
                returnAction = (createChatDb(message)) ? CREATECHATOK:CREATECHATFAIL;
                if(CREATECHATOK.equals(returnAction)) {
                    chatName = message.get(CHATNAME).toString();
                    createChat(chatName, client.getUserName());
                    participants = new ArrayList<>();
                    participants.add(client.getUserName());
                }
                messageClient(returnAction, client, chatName, null, participants);
                break;

            case "chat":
                chatName = message.get(CHATNAME).toString();
                if (GLOBALCHATT.equals(chatName) || chatExists(chatName)) {
                    sendChat(message, client);
                } else {
                    messageClient("msgFAIL", client, null, null, null);
                }
                break;

            case "joinChat":
                returnAction = (joinChat(message, client.getUserName())) ? "joinChatOK":"joinChatFAIL";
                chatName = message.get(CHATNAME).toString();
                messageClient(returnAction, client, chatName, null, null);
                break;

            case "leaveChat":
                chatName = message.get(CHATNAME).toString();
                leaveChat(chatName, client.getUserName());
                break;

            case "joinRandomGame":
                joinRandomGame(client);
                break;

            case "gameChat":
                sendGameChat(message, client);
                break;

            case "startGame":
                startGame(message);
                break;

            case "throwDice":
                throwDice(message);
                break;

            case "movePiece":
                movePiece(message);
                break;

            case "gamePlayed":
                updateUserGamesDb(client.getUserName(), (Boolean) message.get("Won"));
                break;

            case "leaveGame":
                leaveGame(message, client);
                break;

            case REFRESH:
                messageClient(REFRESH, client, null, null, null);
                break;

            case "friendRequest":
                friendRequest(message, client);
                break;

            case "friendAccept":
                List<String> userNames = new ArrayList<>();
                userNames.add(client.getUserName());
                userNames.add(message.get(USERNAME).toString());
                userNames.sort(Collator.getInstance());
                returnAction = (setFriends(userNames))? "friendAcceptOK":"friendAcceptFAIL";
                messageClient(returnAction, client, message.get(USERNAME).toString(), null, null);
                break;

            case "exit":
            case "logout":
                removeFromChatRooms(client.getUserName());
                removeFromGames(client);
                try {
                    synchronized (clientList) {
                        client.close();
                        clientList.remove(client);
                    }
                } catch(IOException e){
                    LOGGER.log(Level.INFO, e.getMessage(), e);
                }
                break;

            case "Test":
                messageClient("testOK", client, null, null, null);
                break;

            default:
                LOGGER.log(Level.SEVERE, "Undefined action received from client: {0}", client.getHostName());
                messageClient("invalidAction", client, null, null, null);
                break;
        }
        // Add user to Global Chat if registered/logged in
        if(returnAction != null && returnAction.matches("newUserOK|loginManualOK|loginAutoOK")) {
            message = new JSONObject();
            message.put(CHATNAME, GLOBALCHATT);
            joinChat(message, client.getUserName());
        }
    }

    /**
     * Sends message to client, parameters are null if not used
     * @param action String, Action of message to be sent
     * @param client ClientConnection, connection to client
     * @param str String, token, userName or null(depends on action)
     * @param str2 String, time left of game-countdown(depends on action)
     * @param list playersJoined in a game or participants of chat(depends on action)
     */
    private void messageClient(String action, ClientConnection client, String str, String str2, List list) {

        JSONObject message = new JSONObject();
        message.put(ACTION, action);

        // Add content to message:
        switch(action) {
            case "newUserOK":
            case "loginManualOK":
                message.put(TOKEN, str);
                break;

            case "loginManualFAIL":
                message.put(MESSAGE, "wrongLogin");
                break;

            case "newUserFAIL":
                message.put(MESSAGE, "userNameTaken");
                break;

            case NAMECHANGEOK:
                client.setUserName(str);
                message.put(USERNAME, str);
                break;

            case NAMECHANGEFAIL:
            case FRIENDREQUESTFAIL:
                message.put(MESSAGE, str);
                break;

            case CREATECHATOK:
                message.put(CHATNAME, str);
                message.put("Participants", list);
                break;

            case "joinChatOK":
                message.put(CHATNAME, str);
                message.put("Participants", getChatParticipants(str));
                break;

            case PLAYERJOINED:
                message.put(GAMEID, Integer.parseInt(str));
                message.put("PlayersJoined", list);
                message.put("Timer", Integer.parseInt(str2));
                break;

            case "friendAdded":
                message.put("Friend", str);
                break;

            case "friendRequestOK":
                message.put(MESSAGE, str);
                message.put(USERNAME, str2);
                break;

            case "friendAcceptOK":
            case "friendAcceptFAIL":
                message.put(USERNAME, str);
                break;

            case REFRESH:
                JSONObject user = getUserFromDb(client.getUserName());
                message.put(NOOFGAMES, user.get(NOOFGAMES).toString());
                message.put(NOOFVICTORIES, user.get(NOOFVICTORIES).toString());
                message.put(GAMES, getTop10Games());
                message.put(VICTORIES, getTop10Victories());
                message.put("ChatNames", getChatNames());
                message.put("Friends", getFriendsFromDb(client.getUserName()));
                break;

            case "testOK":
                message.put(MESSAGE, "This message implies communication ok");
                break;

            default:
                break;
        }
        // Add noOfGames, noOfVictories, top10-lists and image if new user/login valid
        if(action.matches("newUserOK|loginManualOK|loginAutoOK")) {

            JSONObject user = getUserFromDb(str2);

            message.put(USERNAME, str2);
            message.put(NOOFGAMES, user.get(NOOFGAMES).toString());
            message.put(NOOFVICTORIES, user.get(NOOFVICTORIES).toString());
            message.put(GAMES, getTop10Games());
            message.put(VICTORIES, getTop10Victories());
            message.put("ChatNames", getChatNames());
            message.put("GlobalChatParticipants", globalChat.getParticipants());

        }
        // Add image and friends to message if login
        if(action.matches("loginManualOK|loginAutoOK")) {
            message.put("Friends", getFriendsFromDb(str2));
            byte[] imageBytes = getUserImage(str2);

            // If user has an image
            if(imageBytes != null) {
                String byteString = Base64.getEncoder().encodeToString(imageBytes);
                message.put(IMAGE, byteString);
            } else {
                message.put(IMAGE, "");
            }
        }
        // Send message
        try {
            client.send(message.toString());
        } catch(IOException e){
            LOGGER.log(Level.INFO, e.getMessage(), e);
        }
    }

    /**
     * Converts string to JSONObject
     * @param str String to be converted
     * @return JSONObject with str contents
     */
    JSONObject stringToJSON(String str) {

        JSONObject message = null;
        try {
            JSONParser parser = new JSONParser();
            message = (JSONObject) parser.parse(str);
        } catch (ParseException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return message;
    }

    /**
     * Tries to add new user to table Users in DB
     * @param message JSONObject containing Action, UserName and Password
     * @return token if user was added, null otherwise
     */
    String newUser(JSONObject message) {

        String userName = message.get(USERNAME).toString();
        // If user was added to DB:
        try {
            if (addUserToDB(message)) {
                String token = createToken(userName);
                updateUserTokenDb(userName, token);
                return token;
            } else {
                return null;
            }
        } catch(NoSuchAlgorithmException | SQLException e){
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }

    /**
     * Handles login with username and password
     * Sends status back to client
     * @param message JSONObject containing Action, UserName and Password
     * @return token if username/password combination is correct, null otherwise
     */
    private String loginManually(JSONObject message) {

        String userName = message.get(USERNAME).toString();
        String password = message.get(PASSWORD).toString();

        // If user exists and userName/password combination correct
        if(userExists(userName) && validCombination(userName, password)) {
            String token = createToken(userName);
            updateUserTokenDb(userName, token);

            return token;
        } else {
            return null;
        }
    }

    /**
     * Handles login with token from client
     * @param message JSONObject containing Action and Token
     * @return 'loginAutoOK' AND userName if login successful, 'loginAutoFAIL' otherwise
     */
    private String[] loginAutomatically(JSONObject message) {

        String token = message.get(TOKEN).toString();
        Claims claims = decodeToken(token);
        // If token expired or user does not exist in db:
        if (claims == null || !userExists(claims.get("sub").toString())) {
            return new String[] {"loginAutoFAIL"};
        } else {
            return new String[] {"loginAutoOK", claims.get("sub").toString()};
        }
    }

    /**
     * Hashes password with random salt
     * @param password String, password to be hashed
     * @return 2D byte-array containing hash and salt as byte arrays
     * @throws NoSuchAlgorithmException if algorithm does not exist
     */
    private byte[][] hashWithSalt(String password) throws NoSuchAlgorithmException {

        // MessageDigest with SHA-256 algorithm
        MessageDigest md = MessageDigest.getInstance(HASHALGORITHM);

        // Generate random salt
        SecureRandom random = new SecureRandom();
        byte[] saltBytes = new byte[16];
        random.nextBytes(saltBytes);

        // Pass salt to md
        md.update(saltBytes);

        // Generate salted hash
        byte[] hashBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));

        return(new byte[][]{hashBytes, saltBytes});
    }

    /**
     * Creates a JSON Web Token(JWT) valid for 24hours
     * @param userName to be added to token(String)
     * @return String, JWT containing subject, issuedAt and expiration
     */
    String createToken(String userName) {

        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRETKEY);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        // Create Dates for now and for tomorrow (1000*60*60*24 ms is 24 hours):
        long todayMillis = System.currentTimeMillis();
        long tomorrowMillis = System.currentTimeMillis() + 1000*60*60*24;
        Date now = new Date(todayMillis);
        Date tomorrow = new Date(tomorrowMillis);

        // Set JWT Claims
        JwtBuilder builder = Jwts.builder()
                .setIssuedAt(now).setExpiration(tomorrow)
                .setSubject(userName).signWith(signatureAlgorithm, signingKey);

        return builder.compact();
    }

    /**
     * Retrieves token from DB for a given userName
     * @param userName String, userName of user
     * @return Token if it exists, null otherwise
     */
    String getToken(String userName) {

        String query = "SELECT token FROM Users WHERE userName = ?";

        try(PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setString(1, userName);

            try(ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    return rs.getString(TOKEN);
                }
            }
        } catch(SQLException e){
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Decodes JSON Web Token(JWT), checks validity and returns content, or null
     * @param token String (JWT)
     * @return Claims of token or null if token expired
     */
    Claims decodeToken(String token) {

        Claims claims;
        try {
            // Throw exception if it is not a signed JWS (as expected)
            claims = Jwts.parser()
                    .setSigningKey(DatatypeConverter.parseBase64Binary(SECRETKEY))
                    .parseClaimsJws(token).getBody();
        } catch (ExpiredJwtException ignored) {
            return null;
        }
        return claims;
    }

    /**
     * Checks if a username already exists in the database
     * @param userName userName to be checked
     * @return True if userName is found in the DB, false otherwise
     */
    boolean userExists(String userName) {

        String query = "SELECT userName FROM Users WHERE userName = ?";

        try (PreparedStatement stmt = connection.prepareStatement(query)) {

            stmt.setString(1, userName);

            try(ResultSet rs = stmt.executeQuery()) {
                // If query returned empty, userName does not exist in DB:
                if (!rs.next()) {
                    return false;
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        // UserName already exists in DB
        return true;
    }

    /**
     * Checks if combination of username and password is correct
     * @param userName String, username of user
     * @param password String, password of user
     * @return True if combination is correct, false otherwise
     */
    boolean validCombination(String userName, String password) {

        String query = "SELECT userName, salt, password FROM Users WHERE userName = ?";

        try(PreparedStatement stmt = connection.prepareStatement(query)) {

            stmt.setString(1, userName);

            try(ResultSet rs = stmt.executeQuery()) {
                // If query not empty
                if (rs.next()) {
                    Blob dbHashBlob = rs.getBlob(PASSWORD);
                    Blob dbSaltBlob = rs.getBlob("salt");

                    // Convert from blob to byte array and free blobs from memory
                    byte[] hash = dbHashBlob.getBytes(1, (int) dbHashBlob.length());
                    byte[] salt = dbSaltBlob.getBytes(1, (int) dbSaltBlob.length());
                    dbHashBlob.free();
                    dbSaltBlob.free();

                    // Generate hash with same salt
                    MessageDigest md = MessageDigest.getInstance(HASHALGORITHM);
                    md.update(salt);
                    byte[] newHash = md.digest(password.getBytes(StandardCharsets.UTF_8));

                    // Equal passwords generate equal hash when using the same salt
                    return (Arrays.equals(hash, newHash));
                }
            }
        } catch(SQLException | NoSuchAlgorithmException e){
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        return false;
    }

    /**
     * Adds user and its credentials to the database(password is hashed and salted)
     * (Token is updated by updateUserTokenDb)
     * @param credentials JSONObject containing UserName and Password
     * @return True if user was successfully added, false otherwise
     * @throws NoSuchAlgorithmException or SQLException if error occurs
     */
    private boolean addUserToDB(JSONObject credentials) throws NoSuchAlgorithmException, SQLException {

        int added = 0;
        String userName = credentials.get(USERNAME).toString();
        String password = credentials.get(PASSWORD).toString();

        // Hash password
        byte[][] array = hashWithSalt(password);
        byte[] hashBytes = array[0];
        byte[] saltBytes = array[1];

        // Convert to blob for storing in DB
        Blob salt = new SerialBlob(saltBytes);
        Blob hash = new SerialBlob(hashBytes);

        String sql = "INSERT INTO Users(userName, salt, password, noOfGames, noOfVictories)" +
                "VALUES(?,?,?, 0, 0)";

        // If userName does not already exist
        if(!userExists(userName)) {
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {

                // Set parameters
                stmt.setString(1, userName);
                stmt.setBlob(2, salt);
                stmt.setBlob(3, hash);

                added = stmt.executeUpdate();

            } catch (SQLException e) {
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return(added != 0);
    }

    /**
     * Retrieves user from database
     * @param userName String, userName of user
     * @return JSONObject if user exists, null otherwise
     */
    JSONObject getUserFromDb(String userName) {

        JSONObject user = null;

        // If userName exists
        if(userExists(userName)) {
            String query = "SELECT * FROM Users WHERE userName = ?";

            try (PreparedStatement stmt = connection.prepareStatement(query)) {

                stmt.setString(1, userName);

                try(ResultSet rs = stmt.executeQuery()) {

                    if (rs.next()) {
                        user = new JSONObject();
                        user.put(USERNAME, rs.getString(USERNAME));
                        user.put("salt", rs.getString(USERNAME));
                        user.put(PASSWORD, rs.getString(PASSWORD));
                        user.put(TOKEN, rs.getString(TOKEN));
                        user.put(IMAGE, rs.getString("image"));
                        user.put(NOOFGAMES, rs.getInt(NOOFGAMES));
                        user.put(NOOFVICTORIES, rs.getInt(NOOFVICTORIES));
                    }
                }
            } catch (SQLException e) {
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return user;
    }

    /**
     * Removes user and its credentials from the database
     * @param userName String containing users userName
     * @return True if user was removed, false otherwise
     */
    boolean removeUserFromDB(String userName) {

        int removed = 0;
        String sql = "DELETE FROM Users WHERE userName = ?";

        // If user exists:
        if(userExists(userName)) {
            try(PreparedStatement stmt = connection.prepareStatement(sql)) {

                stmt.setString(1, userName);

                removed = stmt.executeUpdate();
            } catch(SQLException e){
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return (removed != 0);
    }

    /**
     * Updates column token in table Users
     * @param userName, String, userName of user
     * @param token String, JSON Web Token(JWT)
     * @return True if update was successful, false otherwise
     */
    private boolean updateUserTokenDb(String userName, String token) {

        int updated = 0;
        String sql = "UPDATE Users SET token = ? WHERE userName = ?";

        // Try updating token
        try(PreparedStatement stmt = connection.prepareStatement(sql)) {

            stmt.setString(1, token);
            stmt.setString(2, userName);

            updated = stmt.executeUpdate();

        } catch(SQLException e){
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        return (updated != 0);
    }

    /**
     * Updates users image
     * @param message JSONObject containing 'Image'
     * @param userName String, name of user
     * @return True if update was successful, false otherwise
     */
    boolean updateUserImage(JSONObject message, String userName) {

        int updated = 0;

        String byteString = message.get(IMAGE).toString();
        // Convert String to byte[]
        byte[] bytes = Base64.getDecoder().decode(byteString);

        String sql = "UPDATE Users SET image = ? WHERE userName = ?";

        try (PreparedStatement stmt = connection.prepareStatement(sql)) {

            // Convert byte-array to blob for storing in DB
            Blob image = new SerialBlob(bytes);

            stmt.setBlob(1, image);
            stmt.setString(2, userName);

            updated = stmt.executeUpdate();

        } catch(SQLException e){
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return(updated != 0);
    }

    /**
     * Increments column noOfGames by 1
     * Increments column noOfVictories by 1 if user won game
     * @param userName String, userName of user
     * @param won Boolean, true if user won the game
     * @return True if update was successful, false otherwise
     */
    boolean updateUserGamesDb(String userName, boolean won) {

        int updated = 0;
        String sql;

        if(won) {
            sql = "UPDATE Users SET noOfGames = noOfGames + 1, noOfVictories = noOfVictories + 1 WHERE userName = ?";
        }else {
            sql = "UPDATE Users SET noOfGames = noOfGames + 1 WHERE userName = ?";
        }

        // Try updating column(s)
        try(PreparedStatement stmt = connection.prepareStatement(sql)) {

            stmt.setString(1, userName);

            updated = stmt.executeUpdate();

        } catch(SQLException e){
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return (updated != 0);
    }

    /**
     * Retrieves a users image from database
     * @param userName String, userName of user
     * @return image as byte-array or null if no image is stored
     */
    byte[] getUserImage(String userName) {

        String query = "SELECT image FROM Users WHERE userName = ?";
        byte[] image = null;

        try(PreparedStatement stmt = connection.prepareStatement(query)) {

            stmt.setString(1, userName);
            try(ResultSet rs = stmt.executeQuery()) {

                if(rs.next()) {

                    Blob blobImage = rs.getBlob("image");
                    if(blobImage != null) {

                        // Convert from blob to byte array and free blob from memory
                        image = blobImage.getBytes(1, (int) blobImage.length());
                        blobImage.free();
                    }
                }
            }
        } catch(SQLException e){
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return image;
    }

    /**
     * Creates a chatroom in database if it does not already exist
     * @param message JSONObject containing 'ChatName'
     * @return True if chatRoom is created, false otherwise
     */
    private boolean createChatDb(JSONObject message) {

        int updated = 0;
        String newChatName = message.get(CHATNAME).toString();

        if(!chatExists(newChatName)) {

            String sql = "INSERT INTO ChatRooms (chatName) VALUES (?)";
            try(PreparedStatement stmt = connection.prepareStatement(sql)) {

                stmt.setString(1, newChatName);

                updated = stmt.executeUpdate();

            } catch(SQLException e){
                LOGGER.log(Level.WARNING, e.getMessage(), e);
            }
        }
        return (updated != 0);
    }

    /**
     * Creates ChatRoom, adds the user who requested it and adds room to list
     * @param chatName String, name of ChatRoom
     * @param userName String, name of user who requested chat-creating
     */
    private void createChat(String chatName, String userName) {

        ChatRoom chatRoom = new ChatRoom(chatName);
        chatRoom.addParticipant(userName);
        chatRoomList.add(chatRoom);
    }

    /**
     * Adds user to chatRoom if it exists
     * @param message JSONObject containing ChatName
     * @param userName String, name of user
     * @return True if user is added, false otherwise
     */
    private boolean joinChat(JSONObject message, String userName) {

        String chatName = message.get(CHATNAME).toString();

        if(chatName.equals(GLOBALCHATT)) {
            // Message all participants, then add user to chat
            informChatParticipants(globalChat, userName, "userJoinedChat");
            globalChat.addParticipant(userName);

            return true;
        } else if(chatExists(chatName)) {

            for(ChatRoom room : chatRoomList) {
                // If ChatRoom has the name and player is not already a participant, add
                if(chatName.equals(room.getName()) && !strInList(room.getParticipants(), userName)) {

                    // Message all participants, then add user to chat
                    informChatParticipants(room, userName, "userJoinedChat");
                    room.addParticipant(userName);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks whether a chatroom already exists in database
     * @param chatName name to be checked
     * @return True if chat already exists, false otherwise
     */
    private boolean chatExists(String chatName) {

        String query = "SELECT chatName FROM Chatrooms WHERE chatName = ?";

        try (PreparedStatement stmt = connection.prepareStatement(query)) {

            stmt.setString(1, chatName);

            try(ResultSet rs = stmt.executeQuery()) {
                // If query returned empty, chatRoom does not exist in DB:
                if (!rs.next()) {
                    return false;
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        // ChatRoom already exists in DB
        return true;
    }

    /**
     * Sends chat-message to all participants of ChatRoom(or Global chat)
     * @param message JSONObject containing ChatName and Message
     * @param client ClientConnection, connection to sender of chat
     */
    private void sendChat(JSONObject message, ClientConnection client) {

        // Add sender to message
        String sender = client.getUserName();
        message.put("Sender", sender);

        String chatName = message.get(CHATNAME).toString();
        String msg = message.get(MESSAGE).toString();
        List participants = null;

        // If message is for global-chat:
        if(GLOBALCHATT.equals(chatName)) {
            participants = globalChat.getParticipants();
        // If message is for other chat:
        } else {
            // Go through ChatRooms, extract list of participants from correct Room
            for (ChatRoom c : chatRoomList) {
                if (c.getName().equals(chatName)) {
                    participants = c.getParticipants();
                    break;
                }
            }
        }

        // If there are any participants
        if(participants != null) {
            // Send message to all participants
            for (ClientConnection c : clientList) {
                if (c.getUserName() != null && strInList(participants, c.getUserName())) {
                    try {
                        c.send(message.toString());
                    } catch (IOException e) {
                        LOGGER.log(Level.INFO, e.getMessage(), e);
                        clientList.remove(c);
                    }
                }
            }
        }
        // Log message
        CHATLOGGER.log(Level.INFO, chatName + ": " + sender + ": " + msg);
    }

    /**
     * Gets participants of a given chat
     * @param chatName String, name of chat
     * @return List of userNames, null if chat does not exist
     */
    private List getChatParticipants(String chatName) {

        List list = null;

        for(ChatRoom room : chatRoomList) {
            if(room.getName().equals(chatName)) {
                list = room.getParticipants();
                break;
            }
        }
        return list;
    }

    /**
     * Removes player from ChatRoom
     * @param chatName String, name of ChatRoom
     * @param userName String, name of user
     */
    private void leaveChat(String chatName, String userName) {

        // Find correct room
        for(ChatRoom room : chatRoomList) {
            if(room.getName().equals(chatName)) {
                // Remove player from participantList and inform other participants
                room.removeParticipant(userName);
                informChatParticipants(room, userName, USERLEFTCHAT);
                break;
            }
        }
    }

    /**
     * Removes a user from all ChatRooms
     * Informs remaining participants that user left
     * @param userName String, name of user
     */
    private void removeFromChatRooms(String userName) {

        // Remove user from globalChat, inform participants
        globalChat.removeParticipant(userName);
        informChatParticipants(globalChat, userName, USERLEFTCHAT);

        // Go through all ChatRooms
        for(ChatRoom room : chatRoomList) {

            // If user participates in ChatRoom
            if(strInList(room.getParticipants(), userName)) {
                // Remove user from ChatRoom and inform participants
                room.removeParticipant(userName);
                informChatParticipants(room, userName, USERLEFTCHAT);
            }
        }
    }

    /**
     * Removes a user from all games
     * Informs remaining players that user left
     * @param clientConnection ClientConnection, the client to be removed
     */
    private void removeFromGames(ClientConnection clientConnection) {
        for (Game game : gameList) {
            if (game.getPlayersJoined().contains(clientConnection.getUserName())) {
                game.removePlayer(clientConnection);
            }
        }
    }

    /**
     * Sends message to all participants of a ChatRoom when a user joins or leaves the ChatRoom
     * @param room ChatRoom
     * @param userName String, name of user who joined or left
     * @param action String, 'userJoinedChat' or 'userLeftChat'
     */
    private void informChatParticipants(ChatRoom room, String userName, String action) {

        // Get participants
        List participants = room.getParticipants();

        // Create message
        JSONObject message = new JSONObject();
        message.put(ACTION, action);
        message.put(CHATNAME, room.getName());
        message.put(USERNAME, userName);

        // Send message to all participants(userLeftChat or userJoinedChat)
        synchronized (clientList) {
            for (ClientConnection client : clientList) {
                if (strInList(participants, client.getUserName())) {
                    try {
                        client.send(message.toString());
                    } catch (IOException e) {
                        LOGGER.log(Level.INFO, e.getMessage(), e);
                    }
                }
            }
        }
    }

    /**
     * Retrieves ChatRooms from database
     * @return List of ChatRooms(null if there is not any)
     */
    private List getChatRoomsFromDb() {

        String query = "SELECT chatName FROM ChatRooms";
        List<ChatRoom> list = null;

        try(PreparedStatement stmt = connection.prepareStatement(query)) {
            try(ResultSet rs = stmt.executeQuery()) {
                if(rs.next()) {
                    list = new ArrayList<>();
                    do {
                        list.add(new ChatRoom(rs.getString("chatName")));
                    } while (rs.next());
                }
            }
        } catch(SQLException e){
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return list;
    }

    /**
     * Retrieves chatNames from chatRoomList
     * @return List containing names of chats, or null if
     */
    private List getChatNames() {

        List<String> list = null;

        // If chatRoomList is not empty
        if(!chatRoomList.isEmpty()) {

            list = new ArrayList<>();

            for (ChatRoom c : chatRoomList) {
                list.add(c.getName());
            }
        }
        return list;
    }

    /**
     * Sends chat-message to all players in game
     * @param message JSONObject containing 'GameID' and 'Message'
     * @param client ClientConnection, connection to client
     */
    private void sendGameChat(JSONObject message, ClientConnection client) {
        // Add sender to message
        message.put("Sender", client.getUserName());
        String gameId = message.get(GAMEID).toString();

        // Find game and get players
        List<String> players = new ArrayList<>();
        for (Game game : gameList) {
            if (game.getGameId() == Integer.parseInt(gameId)) {
                players = game.getPlayersJoined();
                break;
            }
        }

        // Message players
        for (ClientConnection c : clientList) {
            if (strInList(players, c.getUserName())) {
                try {
                    c.send(message.toString());
                } catch (IOException e) {
                    LOGGER.log(Level.INFO, e.getMessage(), e);
                }
            }
        }
    }

    /**
     * Attempts to send friend-request
     * @param message JSONObject containing 'UserToInvite'
     * @param client ClientConnection, connection to client
     */
    private void friendRequest(JSONObject message, ClientConnection client) {

        String userName = client.getUserName();
        String userToInvite = message.get("UserToInvite").toString();

        // If user exists
        if(userExists(userToInvite)) {

            // Sort userNames alphabetically
            List<String> userNames = new ArrayList<>();
            userNames.add(userName);
            userNames.add(userToInvite);
            userNames.sort(Collator.getInstance());

            // Check if a friendship already exists(may be pending)
            try {
                String[] answer = friendshipExists(userNames);

                boolean exists = ("true".equals(answer[0]));
                String status = answer[1];

                // If friendship exists
                if (exists) {

                    // If user has already received invite from the other user
                    if(status.contains(userName)) {
                        if(setFriends(userNames)) {
                            messageClient("friendAdded", client, userToInvite, null, null);
                        } else {
                            messageClient(FRIENDREQUESTFAIL, client, ERROR, null, null);
                        }

                    // If user has already invited user
                    } else if(status.contains(userToInvite)) {
                        messageClient(FRIENDREQUESTFAIL, client, "alreadyInvited", null, null);
                    // If users are already friends
                    } else {
                        messageClient(FRIENDREQUESTFAIL, client, "alreadyFriends", null, null);
                    }

                // No friendship exists
                } else {
                    // Create new friendship wih status pending_userToInvite
                    if(createFriendShip(userName, userToInvite)) {
                        messageClient("friendRequestOK", client, "friendRequestSent", userToInvite, null);
                    } else {
                        messageClient(FRIENDREQUESTFAIL, client, ERROR, null, null);
                    }
                }

            } catch(SQLException e){
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
                messageClient(FRIENDREQUESTFAIL, client, ERROR, null, null);
            }
          // User does not exist
        } else {
            messageClient(FRIENDREQUESTFAIL, client, "userDoesNotExist", null, null);
        }
    }

    /**
     * Checks whether a friendship exists and the status of the friendship
     * UserNames are sorted alphabetically
     * @param userNames List of two userNames sorted alphabetically
     * @return String[2]: 'true' and status of friendship, or 'false' and ''
     * @throws SQLException if error occurs
     */
    private String[] friendshipExists(List<String> userNames) throws SQLException {

        String query = "SELECT * FROM Friendship WHERE userName1 = ? AND userName2 = ?";

        try(PreparedStatement stmt = connection.prepareStatement(query)) {

            stmt.setString(1, userNames.get(0));
            stmt.setString(2, userNames.get(1));

            try(ResultSet rs = stmt.executeQuery()) {

                // If friendship exists(friends or pending)
                if(rs.next()) {
                    return (new String[] {"true", rs.getString("status")});
                } else {
                    return (new String[] {"false", ""});
                }
            }
        }
    }

    /**
     * Creates a friendship in database
     * @param inviter String, userName of user who sent friend request
     * @param userToInvite String, userName of user who received friend request
     * @return True if creation is successful, false otherwise
     * @throws SQLException if error occurs
     */
    private boolean createFriendShip(String inviter, String userToInvite) throws SQLException {

        int added;

        // Sort userNames alphabetically
        List<String> userNames = new ArrayList<>();
        userNames.add(inviter);
        userNames.add(userToInvite);
        userNames.sort(Collator.getInstance());

        String sql = "INSERT INTO Friendship(userName1, userName2, status) VALUES(?, ?, ?)";

        try(PreparedStatement stmt = connection.prepareStatement(sql)) {

            stmt.setString(1, userNames.get(0));
            stmt.setString(2, userNames.get(1));
            stmt.setString(3, "pending_"+userToInvite);

            added = stmt.executeUpdate();
            return(added != 0);
        }
    }

    /**
     * Sets friendship-status to 'friends'
     * @param userNames List of two alphabetically sorted userNames(String)
     * @return True if status updated, false otherwise
     */
    private boolean setFriends(List<String> userNames) {

        String userName1 = userNames.get(0);
        String userName2 = userNames.get(1);
        int updated = 0;

        String sql = "UPDATE Friendship SET status = 'friends' WHERE userName1 = ? AND userName2 = ?";

        try(PreparedStatement stmt = connection.prepareStatement(sql)) {

            stmt.setString(1, userName1);
            stmt.setString(2, userName2);

            updated = stmt.executeUpdate();
        } catch(SQLException e){
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return (updated != 0);
    }

    /**
     * Retrieves friendships from DB
     * @param userName String, name of user
     * @return List of JSONObjects containing 'UserName1', 'UserName2' and 'Status', or null if no friendships
     */
    private List getFriendsFromDb(String userName) {

        String query = "SELECT * FROM Friendship WHERE userName1 = ? OR userName2 = ?";
        List friendships = null;

        try(PreparedStatement stmt = connection.prepareStatement(query)) {

            stmt.setString(1, userName);
            stmt.setString(2, userName);

            try(ResultSet rs = stmt.executeQuery()) {

                friendships = getFriendsAsList(rs);
            }

        } catch(SQLException e){
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return friendships;
    }

    /**
     * Put ResultSet-info into List
     * @param rs ResultSet from executed sql-query, contains columns userName1, userName2 and status
     * @return List of JSONObjects containing the above columns, or null if database is empty
     */
    private List getFriendsAsList(ResultSet rs) {

        List<JSONObject> friends = null;

        try {
            if (rs.next()) {

                friends = new ArrayList<>();
                do {
                    JSONObject friend = new JSONObject();
                    friend.put("UserName1", rs.getString("userName1"));
                    friend.put("UserName2", rs.getString("userName2"));
                    friend.put("Status", rs.getString("status"));
                    friends.add(friend);
                } while (rs.next());

            }
        } catch(SQLException e){
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return friends;
    }

    /**
     * Queries top 10 users who have played the most games
     * @return List of JSONObjects containing userName and noOfGames of top 10 players, or null if database is empty
     */
    private List getTop10Games() {

        List topPlayers = null;
        String query = "SELECT userName, noOfGames FROM Users ORDER BY noOfGames DESC FETCH FIRST 10 ROWS ONLY";

        try(PreparedStatement stmt = connection.prepareStatement(query)) {
            try(ResultSet rs = stmt.executeQuery()) {
                topPlayers = getTop10AsList(rs, NOOFGAMES);
            }
        } catch (SQLException e){
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return topPlayers;
    }

    /**
     * Queries top 10 users who have won the most games
     * @return List of JSONObjects containing userName and noOfVictories of top 10 players, or null if database is empty
     */
    private List getTop10Victories() {

        List topPlayers = null;
        String query = "SELECT userName, noOfVictories FROM Users ORDER BY noOfVictories DESC FETCH FIRST 10 ROWS ONLY";

        try(PreparedStatement stmt = connection.prepareStatement(query)) {
            try(ResultSet rs = stmt.executeQuery()) {
                topPlayers = getTop10AsList(rs, NOOFVICTORIES);
            }
        } catch (SQLException e){
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return topPlayers;
    }

    /**
     * Put ResultSet-info into List
     * @param rs ResultSet from executed query(SQL), contains columns userName and (noOfGames or noOfVictories)
     * @param columnName noOfGames or noOfVictories
     * @return List of JSONObjects containing userName and (noOfGames or noOfVictories), or null if database is empty
     */
    private List getTop10AsList(ResultSet rs, String columnName) {

        List<JSONObject> topPlayers = null;
        try {
            if(rs.next()) {
                topPlayers = new ArrayList<>();
                do {
                    JSONObject player = new JSONObject();
                    player.put(USERNAME, rs.getString(USERNAME));
                    player.put(columnName, rs.getInt(columnName));
                    topPlayers.add(player);
                } while (rs.next());
            }
        } catch(SQLException e){
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        return topPlayers;
    }

    /**
     * Add the user to a list and adds it to a random game
     * @param client Client connection
     */
    private void joinRandomGame(ClientConnection client) {

        String userName = client.getUserName();
        List playerList;
        boolean added = false;

        for (Game game : gameList) {
            playerList = game.getPlayersJoined();
            // If player not already in game
            if (game.getNrOfPlayers() < 4 && !game.hasStarted() && !strInList(playerList, userName)) {

                // Add player to game
                int id = game.getGameId();
                int timer = game.getTimer();
                game.addPlayer(userName, client);
                playerList = game.getPlayersJoined();

                // Message users:
                for (ClientConnection c : clientList) {
                    for (Object name : playerList) {
                        if (c.getUserName().equals(name)) {
                            messageClient(PLAYERJOINED, c, String.valueOf(id), String.valueOf(timer), playerList);
                        }
                    }
                }
                added = true;
                break;
            }
        }
        // Create new game because no games exist or player is in all of them
        if(!added) {
            Game game = new Game(gameIds++);
            game.addPlayer(userName, client);
            game.startTimer();
            playerList = game.getPlayersJoined();
            gameList.add(game);
            messageClient(PLAYERJOINED, client, String.valueOf(gameIds - 1), String.valueOf(0), playerList);
        }
    }

    /**
     * Start a game
     * @param message JSONObject containing players that are in a game
     */
    private void startGame(JSONObject message) {
        List players = (List) message.get("Players");
        for (Game game : gameList) {
            if (game.getGameId() == Integer.parseInt(message.get(GAMEID).toString())) {
                game.startGame(players);
                break;
            }
        }
    }

    /**
     * Throws a dice in the game
     * @param message JSONObject containing a gameID
     */
    private void throwDice(JSONObject message) {
        // Extract gameID from message
        int gameId = Integer.parseInt(message.get(GAMEID).toString());

        for (Game game : gameList) {
            if (game.getGameId() == gameId) {
                game.throwDice();
                break;
            }
        }
    }

    /**
     * Moves a piece in the game
     * @param message JSONObject containing information to move a piece
     */
    private void movePiece(JSONObject message) {
        // Extract gameID from message
        int gameId = Integer.parseInt(message.get(GAMEID).toString());

        for (Game game : gameList) {
            if (game.getGameId() == gameId) {
                game.movePiece(message);
                break;
            }
        }
    }

    /**
     * Removes the player from the game
     * @param message JSONObject containing information used to remove the player
     * @param clientConnection, connection to client
     */
    private void leaveGame(JSONObject message, ClientConnection clientConnection) {
        // Extract gameID from message
        int gameId = Integer.parseInt(message.get(GAMEID).toString());

        for (Game game : gameList) {
            if (game.getGameId() == gameId) {
                game.removePlayer(clientConnection);

                // If the last player leaves then we can remove this game from our list
                if (game.getNrOfPlayers() == 0) {
                    gameList.remove(game);
                }
                break;
            }
        }
    }

    /**
     * Checks whether a string exists in a List of strings
     * @param list List to check in
     * @param str String to look for in list
     * @return True if str is in list, false otherwise
     */
    boolean strInList(List<String> list, String str) {

        // Go through all Strings in list
        for(String s : list) {
            if(s.equals(str)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Changes userName of user if userName available
     * @param message JSONObject containing NewUserName and Password
     * @param userName String, userName of user
     * @return True if userName was changed, false if userName is already taken
     */
    private boolean changeUserName(JSONObject message, String userName) {

        String newUserName = message.get("NewUserName").toString();
        int updated = 0;

        // If username does not already exist
        if(!userExists(newUserName)) {
            String sql = "UPDATE Users SET userName = ? WHERE userName = ?";

            // Try updating column
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {

                stmt.setString(1, newUserName);
                stmt.setString(2, userName);

                updated = stmt.executeUpdate();

            } catch(SQLException e){
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return(updated != 0);
    }

    /**
     * Changes password of user if user exists
     * @param message JSONObject containing Password and NewPassword
     * @param userName String, userName of user
     * @return True if password was changed, false otherwise
     */
    private boolean changePassword(JSONObject message, String userName) {

        String password = message.get(PASSWORD).toString();
        String newPassword = message.get("NewPassword").toString();
        int updated = 0;

        // If user exists and username/oldPassword combination correct
        if(userExists(userName) && validCombination(userName, password)) {

            String sql = "UPDATE Users SET password = ?, salt = ? WHERE userName = ?";

            try(PreparedStatement stmt = connection.prepareStatement(sql)) {
                // Hash password
                byte[][] array = hashWithSalt(newPassword);
                byte[] hashBytes = array[0];
                byte[] saltBytes = array[1];

                // Convert hash and salt to blob for storing in DB
                Blob hash = new SerialBlob(hashBytes);
                Blob salt = new SerialBlob(saltBytes);

                // Set parameters
                stmt.setBlob(1, hash);
                stmt.setBlob(2, salt);
                stmt.setString(3, userName);

                updated = stmt.executeUpdate();

            } catch(NoSuchAlgorithmException | SQLException e){
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return (updated != 0);
    }

    /**
     * Main, launches server
     * @param args contains the supplied command-line arguments
     */
    public static void main(String[] args) {
        // Start server
        new Server(null);
    }
}