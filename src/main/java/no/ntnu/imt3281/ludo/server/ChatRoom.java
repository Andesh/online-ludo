package no.ntnu.imt3281.ludo.server;

import java.util.ArrayList;
import java.util.List;

/**
 * A chatroom with id and participants
 */
class ChatRoom {
    private String chatName;
    private List<String> participants;

    /**
     * Constructor, sets chatId and creates list of participants
     * @param name String, chatName of chat
     */
    ChatRoom(String name) {
        chatName = name;
        participants = new ArrayList<>();
    }

    /**
     * @return id of chat
     */
    String getName(){
        return chatName;
    }

    /**
     * @return list of chat-participants
     */
    List getParticipants(){
        return participants;
    }

    /**
     * Adds participant to chatroom
     * @param userName String, name of new participant
     */
    void addParticipant(String userName){
        participants.add(userName);
    }

    /**
     * Removes participant from chatroom
     * @param userName String, name of participant
     */
    void removeParticipant(String userName){
        if(userName != null) {
            for (String p : participants) {
                if (userName.equals(p)) {
                    participants.remove(p);
                    break;
                }
            }
        }
    }
}
