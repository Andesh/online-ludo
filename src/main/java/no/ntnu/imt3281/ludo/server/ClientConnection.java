package no.ntnu.imt3281.ludo.server;

import no.ntnu.imt3281.ludo.logic.*;
import org.json.simple.JSONObject;

import java.io.*;
import java.net.Socket;

/**
 * Connection to a client.
 * Server can communicate with its clients through this class.
 */
public class ClientConnection implements DiceListener, PlayerListener, PieceListener {

    private BufferedReader br;
    private BufferedWriter bw;
    private String hostName;
    private String userName;

    // Used for adding/extracting information to/from server-messages
    private static final String ACTION = "Action";
    private static final String EVENT = "event";
    private static final String EVENTTYPE = "EventType";
    private static final String GAMEID = "GameID";

    /**
     * Constructor.
     * @param s ServerSocket, connection to a client
     * @throws IOException if creating reader/writer fails
     */
    ClientConnection(Socket s, String hostName) throws IOException {

        br = new BufferedReader(new InputStreamReader(s.getInputStream()));
        bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
        this.hostName = hostName;
        userName = null;
    }

    void setUserName(String userName){
        this.userName = userName;
    }

    /**
     * @return Name of Client
     */
    String getUserName(){
        return userName;
    }

    /**
     * @return Hostname of client
     */
    String getHostName(){
        return hostName;
    }

    /**
     * Used by server to read a message from the client.
     * @return message if there is any, null otherwise
     * @throws IOException if reading fails
     */
    String read() throws IOException {

        if(br.ready()){
            return br.readLine();
        }
        return null;
    }

    /**
     * Used by server to send a message to the client.
     * @param s String, message to send
     * @throws IOException if sending message fails
     */
    void send(String s) throws IOException {
        bw.write(s);
        bw.newLine();
        bw.flush();
    }

    /**
     * Closes communication-channels.
     * @throws IOException if close() fails
     */
    void close() throws IOException {
        bw.close();
        br.close();
    }

    /**
     * Gets a dice event and retrieves data from it, then sends it to the client
     * @param diceEvent Dice event
     */
    @Override
    public void diceThrown(DiceEvent diceEvent) {
        // Construct dice event-message
        JSONObject message = new JSONObject();
        message.put(ACTION, EVENT);
        message.put(EVENTTYPE, "diceEvent");
        message.put(GAMEID, diceEvent.getId());
        message.put("DiceNumber", diceEvent.getDiceNumber());
        try {
            send(message.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets a player event and retrieves data from it, then sends it to the client
     * @param playerEvent Player event
     */
    @Override
    public void playerStateChanged(PlayerEvent playerEvent) {
        // Construct player event-message
        JSONObject message = new JSONObject();
        message.put(ACTION, EVENT);
        message.put(GAMEID, playerEvent.getId());
        message.put(EVENTTYPE, "playerEvent");
        message.put("ActivePlayer", playerEvent.getActivePlayer());
        message.put("Status", playerEvent.getStatus());

        try {
            send(message.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets a piece event and retrieves data from it, then sends it to the client
     * @param pieceEvent Piece event
     */
    @Override
    public void pieceMoved(PieceEvent pieceEvent) {
        // Construct piece event-message
        JSONObject message = new JSONObject();
        message.put(ACTION, EVENT);
        message.put(GAMEID, pieceEvent.getId());
        message.put(EVENTTYPE, "pieceEvent");
        message.put("ActivePlayer", pieceEvent.getActivePlayer());
        message.put("PieceNumber", pieceEvent.getPieceNumber());
        message.put("FromPos", pieceEvent.getFromPosition());
        message.put("ToPos", pieceEvent.getToPosition());

        try {
            send(message.toString());
        } catch (IOException e) {}
    }
}
