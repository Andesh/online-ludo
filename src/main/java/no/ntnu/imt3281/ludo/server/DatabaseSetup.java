package no.ntnu.imt3281.ludo.server;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Has functionality for creating database-tables and setting database-password
 */
class DatabaseSetup {

    private static final Logger LOGGER = MyLogger.getLogger();

    /**
     * Creates tables Users and Chatrooms in DB
     * @param connection to DB
     */
    static void createTables(Connection connection){

        try (Statement stmt = connection.createStatement()){
            // Create Users-table
            stmt.execute("CREATE TABLE Users(" + // NOSONAR
                    "userName varchar(20) NOT NULL," +
                    "salt blob," +
                    "password blob," +
                    "token varchar(170)," +
                    "noOfGames int," +
                    "noOfVictories int," +
                    "image blob DEFAULT null," +
                    "PRIMARY KEY(userName))");

            // Create Chatrooms-table
            stmt.execute("CREATE TABLE Chatrooms(" + // NOSONAR
                    "chatName varchar(20)," +
                    "PRIMARY KEY(chatName))");

            // Create Friendship-table
            stmt.execute("CREATE TABLE Friendship(" + // NOSONAR
                    "userName1 varchar(20) NOT NULL," +
                    "userName2 varchar(20) NOT NULL," +
                    "status varchar(28) NOT NULL," +
                    "PRIMARY KEY(userName1, userName2))");

        } catch(SQLException e){
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            System.exit(-1);
        }
    }

    /**
     * Sets password for db
     * @param connection, connection to DB
     * @param password String, password to be set
     */
    static void setDBPassword(Connection connection, String password){

        try(Statement stmt = connection.createStatement()){

            stmt.executeUpdate("CALL SYSCS_UTIL.SYSCS_SET_DATABASE_PROPERTY(\n" // NOSONAR
                    + "    'derby.connection.requireAuthentication', 'true')");
            stmt.executeUpdate("CALL SYSCS_UTIL.SYSCS_SET_DATABASE_PROPERTY(\n"
                    + "    'derby.authentication.provider', 'BUILTIN')");
            stmt.executeUpdate("CALL SYSCS_UTIL.SYSCS_SET_DATABASE_PROPERTY(\n"
                    + "    'derby.user.root', '"+password+"')");
            stmt.executeUpdate("CALL SYSCS_UTIL.SYSCS_SET_DATABASE_PROPERTY(\n"
                    + "    'derby.database.propertiesOnly', 'true')");
        } catch(SQLException e){
            LOGGER.log(Level.INFO, e.getMessage(), e);
        }
    }
}
