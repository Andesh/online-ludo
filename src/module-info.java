module no.ntnu.imt3281.ludo {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.util;
    requires java.logging;
    requires java.sql;
    requires org.json;

    opens no.ntnu.imt3281.ludo to javafx.fxml;
    exports no.ntnu.imt3281.ludo;
}