﻿# Documentation
Have a look at the documentation [here](https://bitbucket.org/Andesh/online-ludo/wiki/).

## Team members
Anders Hoelseth Rebner
 
Vegard Elgesem Kostveit

Petter Sagvold

Pål Anders Owren

## Other
Project based on [this repo](https://bitbucket.org/okolloen/imt3281-project2-2019/wiki/Home)

[Trello-board used](https://trello.com/b/K92KI7wU/ludo)